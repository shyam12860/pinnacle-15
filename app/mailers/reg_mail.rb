class RegMail < ActionMailer::Base
  default from: "qms@smeannauniv.org"

  def event_email(user)
  	@user = user
  	mail(to: @user[:email], subject: 'Pinnacle 2014 - ' + @user[:eventname])
  end

  def workshop_email(user)
  	@user = user
  	mail(to: @user[:email], subject: 'Pinnacle 2014 - ' + @user[:eventname])
  end

  def stuamb_email(user)
    @user = user
    mail(to: @user[:email], subject: 'Pinnacle 2014 - PiAmbassador Program')
  end

  def test_mail
  	 mail(to: "data.chand@gmail.com", subject: 'Welcome to My Awesome Site', body: "Test")
  end
end
