class Slider < ActiveRecord::Base


	has_attached_file :image, :styles => {:small => "150x150>", :medium => "940x360>", :large => "1024x900>", :thumbnail => "150x150>"},
							 :url  => "/slider/images/:id_:style.jpg",
                  			 :path => ":rails_root/public/slider/images/:id_:style.jpg"

end
