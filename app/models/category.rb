class Category < ActiveRecord::Base

	has_many :events
	has_many :workshops

	after_create :update_map


	protected

	def update_map

		map = Map.new
		map.category = "category"
		map.item_id = self.id
		map.url = self.url
		map.save

	end




	
end
