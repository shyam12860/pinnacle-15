class Sponsor < ActiveRecord::Base


	has_attached_file :image,:styles => {:sponsor => "300x", :thumbnail => "150x150>"},
							 :url  => "/assets/images/:id_:style.png",
                  			 :path => ":rails_root/public/assets/images/:id_:style.png"

end
