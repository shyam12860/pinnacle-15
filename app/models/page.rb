class Page < ActiveRecord::Base
	after_create :create_map
	protected
	def create_map
		map = Map.new
		map.category = "page"
		map.item_id = self.id
		map.url = File.join('/',self.url)
		map.save
	end
end
