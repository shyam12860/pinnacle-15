class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
   devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable, :omniauth_providers => [:google_oauth2 , :facebook]
  has_one :profile
  has_many :registration
  has_many :events
  has_many :workshops
  # has_one :rebus

  after_create :create_profile_record

    def self.from_omniauth(auth)
    puts auth
    if user = User.find_by_email(auth.info.email)
      user.provider = auth.provider
      user.uid = auth.uid
      return user
    else
      user = User.where(:provider => auth.provider, :uid => auth.uid).first
      if user
        return user
      else
        user = User.new
        user.provider = auth.provider
        user.uid = auth.uid
        user.email = auth.info.email
        user.oauth_token=auth.credentials.token
        user.oauth_expires =auth.credentials.expires_at 
        user.password = Devise.friendly_token[0,20]
        user.save
        #user.avatar = auth.info.image
        if user.save
          user.profile.name = auth.info.name
          user.profile.gender = auth.extra.raw_info.gender
          #user.profile.avatar = auth.info.image
          user.profile.save
        end
        return user
      end
      
    end
  end

  
  def self.find_for_facebook_oauth(auth, signed_in_resource=nil)
    user = User.where(:provider => auth.provider, :uid => auth.uid).first
    if user
      return user
    else
      registered_user = User.where(:email => auth.info.email).first
      if registered_user
        return registered_user
      else
      	puts auth
        user = User.create( 
                            provider:auth.provider,
                            uid:auth.uid,
                            email:auth.info.email,
                            password:Devise.friendly_token[0,20],
                            oauth_expires:auth.credentials.expires_at,
                            oauth_token:auth.credentials.token
                          )
        user.profile.name = auth.extra.raw_info.name
        user.profile.gender = auth.extra.raw_info.gender
        #user.profile.avatar = auth.info.iamge
        user.profile.save
        user
      end
       
    end
  end

  def create_profile_record
  	Profile.create :user_id => self.id, :pinnacleid => "PIN" + SecureRandom.hex(2)
    self.role = 'student'
    self.save
  end
# ----------------------Cancan Authorization --------------------------------------#
  def super_admin?
    self.role=="super_admin"
  end
  def admin?
    self.role=="admin"
  end
  def event_admin?
    self.role=="event_admin"
  end
    def page_admin?
    self.role=="page_admin"
  end
  def workshop_admin?
    self.role=="workshop_admin"
  end
  def student?
    self.role=="student"
  end
  # def role?(base_role)
  #   ROLES.index(base_role.to_s) <= ROLES.index(role)
  # end
 
end
