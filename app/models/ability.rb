class Ability
  include CanCan::Ability

  def initialize(user)
   
        if user.super_admin? 

         can :manage, CmsEventController
         can :manage, CmsUpdateController
         can :manage, CmsPageController
         can :manage, CmsMainController
         can :manage, ManageuserController
         can :manage, CmsWorkshopController
         can :manage, CmsSliderController
         can :manage, CmsCategoryController
         can :manage, ManageuserController
         can :manage, CmsSponsorController
        
        elsif user.admin? 

         can :manage, CmsEventController
         can :manage, CmsMainController                  
         can :manage, CmsPageController
         can :manage, CmsWorkshopController
         can :manage, CmsSponsorController

        elsif user.event_admin?
         
         can :index, CmsEventController
         can :show, CmsEventController
         can :update, CmsEventController
         cannot :delete, CmsEventController
         cannot :save, CmsEventController
         can :manage , CmsMainController

        elsif user.page_admin?

         can :index, CmsPageController
         can :show, CmsPageController
         can :update, CmsPageController
         cannot :delete, CmsPageController
         cannot :save, CmsPageController
         can :index , CmsMainController

        elsif user.workshop_admin?

         can :index, CmsWorkshopController
         can :show, CmsWorkshopController
         can :update, CmsWorkshopController
         cannot :delete, CmsWorkshopController
         cannot :save, CmsWorkshopController
         can :manage , CmsMainController

        elsif user.student?
             
        end
    end 
  
end
