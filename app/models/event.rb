class Event < ActiveRecord::Base
	after_create :update_map

	belongs_to :category
	belongs_to :user

	def cover_url
		self.cover_image.url
	end

	def self.get_all(id)
		self.find(id).select([:url])
	end
	
	protected 
	def update_map 

		map = Map.new
		map.category = "event"
		map.item_id = self.id
		map.url = self.url
		map.save 

	end 

	has_attached_file :cover_image, :styles => {:small => "940"},
									:url => "/uploads/cover/:id_:style_event.:extension",
  									:path => ":rails_root/public/uploads/cover/:id_:style_event.:extension"
end
