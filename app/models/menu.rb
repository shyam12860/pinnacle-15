class Menu < ActiveRecord::Base

	belongs_to :parent, class_name: 'Menu'
	has_many :sub_menus , dependent: :destroy , foreign_key: 'parent_id', class_name: 'Menu' 

	def backtrack
		self.parent.title+' --> ' 
	end
end
