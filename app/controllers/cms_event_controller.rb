class CmsEventController < ApplicationController

  layout "layouts/cms"

  before_filter :authenticate_user!
 authorize_resource :class => CmsEventController


  def index
    @category = Category.where(primary: 'event')
  end

  def show
    if(params[:id])

      event = {:image_url => Event.find(params[:id]).cover_image.url(:small)}

      render :json => event
    else
      render :json => Event.all
    end
  end


  def update
    event = Event.find(params[:id])

    map = Map.where({item_id: event.id,category: 'event'}).first
    map.url = File.join('/',params[:url])
    map.save

    event.name = params[:name]
    event.venue = params[:venue]
    event.url = params[:url]
    event.description = params[:description]
    event.start_date = params[:start_date]
    event.tabs = params[:tabs].to_json
    event.meta = params[:meta].to_json
    event.end_date = params[:end_date]
    event.category_id = params[:category_id]


    if params[:registration_required] == true
      event.registration_required = true
    else
      event.registration_required = false
    end

    if event.save
      render :json => event
    end
   

  end

  def delete
        event = Event.find(params[:id])
    if event.delete
      render :json => event
    end
    
  end

  def save
    event = Event.new
    event.name = params[:name]
    event.venue = params[:venue]
    event.url = File.join('/',params[:url])

    event.tabs = params[:tabs].to_json
    event.meta = params[:meta].to_json
    event.description = params[:description]
    event.start_date = params[:start_date]
    event.end_date = params[:end_date]
    event.category_id = params[:category_id]

    if params[:registration_required]
      event.registration_required = true
    else
      event.registration_required = false
    end

    if event.save
      render :json => event
    end
  end

  def checkurl

    map = Map.where({'url' => File.join('/',params[:url])})

    if map.empty?
      render :json => {:status => false}
    else
      render :json => {:status => true}
    end

  end

  def file_upload 
  
    event = Event.find(params[:event_id])
    event.cover_image = params[:cover_image]
    
    if event.save
      flash[:notice] = "Cover Image Uploaded for #{event.name}"
      redirect_to event_cms_path
    end

  end


end
