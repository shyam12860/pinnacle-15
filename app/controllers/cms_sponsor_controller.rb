class CmsSponsorController < ApplicationController
  
  layout "layouts/cms"

  before_filter :authenticate_user!
  authorize_resource :class => CmsSponsorController

  def index
  end


  def save
          
    sponsor = Sponsor.new
    sponsor.title = params[:title]
    sponsor.category = params[:category]
    sponsor.url = params[:url]
    sponsor.image = params[:myFile]
    sponsor.save

  sponsor = Sponsor.last
  sponsor.image_path = sponsor.image.url(:thumbnail)
    
    if sponsor.save
      flash[:notice] = "Image Uploaded for " + sponsor.title
      render :json => sponsor
    end

  end

  def show
      sponsor = Sponsor.all
      render :json => sponsor
  end

  def update

  end

  def delete

    sponsor = Sponsor.find(params[:id])

    sponsor.image.destroy
    sponsor.save

    sponsor = Sponsor.find(params[:id])

    sponsor.delete

  end

  def file_upload
  end

end
