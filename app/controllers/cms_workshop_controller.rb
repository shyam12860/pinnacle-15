class CmsWorkshopController < ApplicationController
  
  before_filter :authenticate_user!
  authorize_resource :class => CmsWorkshopController
  
  layout "layouts/cms"

  def index
    @category = Category.where(secondary: 'workshop')
  end

  def save
    workshop = Workshop.new
    workshop.name = params[:name]
    workshop.venue = params[:venue]
    workshop.url = File.join('/',params[:url])
    workshop.tabs = params[:tabs].to_json
    workshop.meta = params[:meta].to_json
    workshop.description = params[:description]
    workshop.time = params[:time]
    workshop.start_date = params[:start_date]
    workshop.end_date = params[:end_date]

    if workshop.save
      render :json => workshop
    end
  end

  def show
    if(params[:id])

      workshop = {:image_url => Workshop.find(params[:id]).cover_image.url(:small)}

      render :json => workshop
    else
      render :json => Workshop.all
    end
  end

  def update
    workshop = Workshop.find(params[:id])

    map = Map.where({item_id: workshop.id, category: 'workshop'}).first
    map.url = File.join('/',params[:url])
    map.save

    workshop.name = params[:name]
    workshop.venue = params[:venue]
    workshop.url = File.join('/',params[:url])
    workshop.tabs = params[:tabs].to_json
    workshop.meta = params[:meta].to_json
    workshop.description = params[:description]
    workshop.time = params[:time]
    workshop.start_date = params[:start_date]
    workshop.end_date = params[:end_date]

    if workshop.save
      render :json => workshop
    end
  end

  def delete
    workshop = Workshop.find(params[:id])
    
    if workshop.delete
      render :json => workshop
    end

  end

  def checkurl

    map = Map.where({'url' => File.join('/',params[:url])})

    if map.empty?
      render :json => {:status => false}
    else
      render :json => {:status => true}
    end

  end

  def file_upload 
  
    workshop = Workshop.find(params[:workshop_id])
    workshop.cover_image = params[:cover_image]
    
    if workshop.save
      flash[:notice] = "Cover Image Uploaded for #{workshop.name}"
      redirect_to workshop_cms_path
    end

  end
end
