class CmsRebusController < ApplicationController
  
  layout "layouts/cms"

  require 'json'

  def index
  end

  def show
    
    num = Rebusqna.count/7

    s = []

    num.times do |n|

      r = Rebusqna.where(round: n+1)

      s[n] = r

    end

    render :json => s
  end

  def save

    num = Rebusqna.count/7
    num = num + 1

    s = []

    7.times do |n|
      
      r = Rebusqna.new
      r.round = num
      r.save

      s[n] = r

    end
    
    

    render :json => s

  end

  def update

    r = Rebusqna.find(params[:id])

    r.close_answer = params[:close_answer].downcase
    r.answer = params[:answer].downcase
    r.question = params[:question]

    if r.save
    
      render :json => r

    end

  end

  def delete
  end
end
