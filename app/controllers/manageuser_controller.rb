class ManageuserController < ApplicationController

	before_filter :authenticate_user!
	authorize_resource :class => ManageuserController
	def index

	end

	def show
		users = User.all.select("id,email,role")
		events = Event.all.select("id,name")
		workshops = Workshop.all.select("id,name")
		temp = {users: users, events: events, workshops: workshops}
		render :json => temp
	end

	def update
		user = User.find params[:id]
		user.email = params[:email]
		# user.username = params[:username]
		user.role = params[:role]
		user.save
		render :json => user
	end

	def delete
		user = User.find params[:id]
		user.delete

		render :json => user
	end

	def assign
		
		if(params[:event] != 999)
		event = Event.find(params[:event])	
		event.user_id = params[:id]
		event.save
		end
		if(params[:workshop] != 999)
		workshop = Workshop.find(params[:workshop])
		workshop.user_id = params[:id]
		workshop.save
		end
		
	end
end
