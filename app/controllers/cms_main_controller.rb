class CmsMainController < ApplicationController
  require 'csv'
  layout "layouts/cms"

  before_filter :authenticate_user!
  authorize_resource :class => CmsMainController

  def index
  	@event = Event.count
  	@workshop = Workshop.count
  	@user = User.count
  	#@sponsor =  Sponsor.count
  end

  def dash
  end

  def menu
  	@menus = Menu.all.order(order: :asc)
  	@top_menu = Menu.where(parent_id: nil).order(order: :asc)
  	@maps = Map.all

  end

  def save_menu
  	menu = Menu.new
  	menu.title = params[:title]
  	menu.url = params[:url]
  	menu.parent_id = params[:parent_id]
    menu.level = params[:level]
    menu.category = params[:category]
    menu.order = params[:order]
  	if menu.save
  		flash[:notice] = 'Menu added'
  		redirect_to menu_path and return
  	else
  		flash[:alert] = 'There was a problem..sorry'
  		redirect_to menu_path and return
  	end
  end

  def delete_menu
  	menu = Menu.find(params[:id]).delete
  	flash[:notice] = 'Menu Deleted'
  	redirect_to menu_path and return
  end

  def viewreg
   
   @all_events = Event.all
   @all_workshops = Workshop.all

   @paramscategory = params[:category] || ""
   @paramsid = params[:id] || ""

      if request.post?
        if params[:category] == "event"
          if params[:id].nil?
            @reg = Registration.where(category: params[:category])
          else
            @reg = Registration.where(category: params[:category],item_id: params[:id])
          end
        elsif params[:category] == "workshop"
          if params[:id].nil?
            @reg = Registration.where(category: params[:category])
          else
            @reg = Registration.where(category: params[:category],item_id: params[:id])
          end
        end
      else
        @reg = Registration.all
      end
 

  end

  def viewxls
    @all_events = Event.all
   @all_workshops = Workshop.all

   @paramscategory = params[:category] || ""
   @paramsid = params[:id] || ""

      if request.post?
        if params[:category] == "event"
          if params[:id].nil?
            @reg = Registration.where(category: params[:category])
          else
            @reg = Registration.where(category: params[:category],item_id: params[:id])
          end
        elsif params[:category] == "workshop"
          if params[:id].nil?
            @reg = Registration.where(category: params[:category])
          else
            @reg = Registration.where(category: params[:category],item_id: params[:id])
          end
        end
      else
        @reg = Registration.all
      end
      
    respond_to do |format|
      format.xls
    end
  end

  def delete_reg
    registration = Registration.find(params[:id])

    if registration.delete
      redirect_to :viewreg
    end

  end

  def changepayment
    registration = Registration.find(params[:id])

    registration.payment = "paid"
    
    if registration.save
        redirect_to :viewreg
    end
  end


  def piambassador
    @amb = Profile.where(piamb: 1)
  end

  def viewpi
    @subuser = Profile.where(referralid: params[:referralid])
  end
end
