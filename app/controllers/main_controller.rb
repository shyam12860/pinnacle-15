class MainController < ApplicationController
  layout "layouts/main"
  before_filter :authenticate_user!, :only => [:getregistration,:referral,:saveprofile,:getprofile]
 
# load_and_authorize_resource :class => MainController
 
  def index
  end

  def home
  end

  def updates
      render :json => Update.all
  end

  def slides
    slider = Slider.all
    render :json => slider
  end

  def category
    render :json => Category.all
  end

  def menu
    render :json => Menu.all
  end

  def profile    
  end

  def sponsors
    render :json => Sponsor.all
  end

  def saveprofile
    @person = current_user.profile
    @person.name = params[:name] || ""
    @person.course = params[:course] || ""
    @person.branch = params[:branch] || ""
    @person.institution = params[:institution] || ""
    @person.year = params[:year] || ""
    @person.phonenumber = params[:phonenumber] || ""
    @person.how = params[:how] || ""
    @status = ""

    if (@person.name != "" and @person.course != "" and @person.branch != "" and @person.institution != "" and @person.phonenumber != "" and @person.year != "")
      @person.profilecomplete = 1
    else 
      @person.profilecomplete = 0
    end

    


    if @person.how == "3"
      if current_user.profile.referralid.blank?
        @person.referralid = params[:referralid] || ""

        if @person.referralid.blank?
          @message = "Please Provide a Pi Ambassador ID"
          render :json => {status: @message} and return
        else
          check = Profile.where(piamb: "1", pinnacleid: @person.referralid).first
          if check
            @status = "Success"
            @message = "The Pi ID has been added to your account"
          else
            @person.referralid = ""
            @message = "The Given Referral ID is not valid"
            render :json => {status: @message} and return
          end
        end
      else
        @message = "We have detected a Referral in your Account. "
      end
    end

    if @person.save
      @message = "Profile saved Successfully"
    else
      @message = "Error"
      render :json => {status: @message} and return
    end

        render :json => {status: @message, op: @status} and return
  end

  def getprofile
    @profile = current_user.profile
    render :json => @profile
  end

  def getregistration

    if params[:category] == "event"
      if params[:item_id] == "18"
        @stuamb = Profile.where(pinnacleid: current_user.profile.pinnacleid).first
        @stuamb.piamb = "1"

        user = {name: current_user.profile.name, email: current_user.email, pinnacleid: current_user.profile.pinnacleid}
        if @stuamb.save
          RegMail.stuamb_email(user).deliver
          render :json => {status: "Success"}
        else
          render :json => {status: "Error"}
        end
      else
        @registration = Registration.new
        @registration.user_id = current_user.id
        @registration.item_id = params[:item_id]
        @registration.category = "event"
        
        @eventdetails = Event.where(id: params[:item_id]).first

        user = {name: current_user.profile.name, eventname: @eventdetails.name, email: current_user.email}

        if @registration.save
          RegMail.event_email(user).deliver
          render :json => {status: "Success"}
        else
          render :json => {status: "Error"}
        end
      end
    else
      @registration = Registration.new
      @registration.user_id = current_user.id
      @registration.item_id = params[:item_id]
      @registration.category = "workshop"
      workshop = {number: params[:ddnumber], date: params[:dddate], bank: params[:ddbank], branch: params[:ddbranch]}
      @registration.details = workshop.to_json
      @eventdetails = Workshop.where(id: params[:item_id]).first

      user = {name: current_user.profile.name, eventname: @eventdetails.name, email: current_user.email}
      if @registration.save
        RegMail.workshop_email(user).deliver
        render :json => {status: "Success"}
      else
        render :json => {status: "Error"}
      end
    end
  end

  def page
    id0 = (params[:id0] && params[:id0]) || ""
    id1 = (params[:id1] && params[:id1]) || ""
    id2 = (params[:id2] && params[:id2]) || ""
    id3 = (params[:id3] && params[:id3]) || ""
    id4 = (params[:id4] && params[:id4]) || ""
    id5 = (params[:id5] && params[:id5]) || ""
    id6 = (params[:id6] && params[:id6]) || ""
    id7 = (params[:id7] && params[:id7]) || ""

    @path = File.join('/',id0,id1,id2,id3,id4,id5,id6,id7).chop

  	@map = Map.where(url: @path).first
  	if @map
  		@category = @map.category
      
      if @category == 'event'
        @event = Event.find @map.item_id
        @cover = Event.find(@map.item_id).cover_image.url(:small)

        if user_signed_in? 
          @registration = Registration.where(user_id: current_user.id,item_id:  @map.item_id, category: "event").first
        else 
          @registration = false
        end
         #render corresponding template
      elsif @category == 'workshop'
        @event = Workshop.find @map.item_id
        @cover = Workshop.find(@map.item_id).cover_image.url(:small)
        if user_signed_in?
          @registration = Registration.where(user_id: current_user.id,item_id:  @map.item_id, category: "workshop").first
        else 
          @registration = false
        end
      elsif @category == 'page'
        @page = Page.find @map.item_id
        #render corresponding template
      end

  	else
  		@title = '404'
       #render 404
  	end
  end

  def referral
    check = Profile.where(:pinnacleid => params[:referralid], :piamb => "1")

    if check.blank?
      @message = "This PINNACLE ID does not support Referral Program."
    else 
      if user_signed_in?
        @message = current_user.profile.pinnacleid       

        profile = Profile.where(:pinnacleid => current_user.profile.pinnacleid).first

        if profile.referralid.blank?
          profile.referralid = params[:referralid]
          if profile.save
            @message = "Referral ID ("+params[:referralid]+") has been added to your PINNACLE ID ("+current_user.profile.pinnacleid+") "
          else
            @message = "No Referral ID has been added PINNACLE ID ("+current_user.profile.pinnacleid+") "
          end
        else
           @message = "We have detected a Referral ID already attached with your PINNACLE ID ("+current_user.profile.pinnacleid+") "
        end
      else
         @message = "Sign In"
      end
    end
  end
end
