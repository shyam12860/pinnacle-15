class CmsSettingsController < ApplicationController
  
  layout "layouts/cms"

  before_filter :authenticate_user!
 authorize_resource :class => CmsSettingsController
 
  def index
  end

  def show
  	map = Map.all

  	render :json => map
  end
end
