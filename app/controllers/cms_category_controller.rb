class CmsCategoryController < ApplicationController

  layout "layouts/cms"

  before_filter :authenticate_user!
 authorize_resource :class => CmsCategoryController

  def index
  end

  def show
    event = Category.where({primary: 'event'})
    workshop = Category.where({primary: 'workshop'})
    page = Category.where({primary: 'page'})

    cat = {:event => event, :workshop => workshop, :page => page}

    render :json => cat

  end

  def save
    cat = Category.new

    cat.primary = params[:primary]
    cat.secondary = params[:secondary]
    cat.url = File.join('/',params[:url])

    if cat.save
      render :json => cat
    end

  end

  def checkurl

    map = Map.where({'url' => File.join('/',params[:url])})

    if map.empty?
      render :json => {:status => false}
    else
      render :json => {:status => true}
    end

  end

  def update
  end

  def delete
    cat = Category.find(params[:id])
    
    url = cat.url
    
    cat.delete
    
    map = Map.where({url: url}).first
    

    if map.delete
      render :json => cat
    end
  end
end
