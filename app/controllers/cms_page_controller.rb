class CmsPageController < ApplicationController

  layout "layouts/cms"


 before_filter :authenticate_user!
authorize_resource :class => CmsPageController

  def index
  end

  def show
    if(params[:id])
      render :json => Page.find(params[:id])
    else
      render :json => Page.all
    end
  end
  
  def save
    title = params[:title]
    url = params[:url]
    content = params[:content]
    plugin = params[:plugin]
    page = Page.new
      page.title = title
      page.url = File.join('/',params[:url])
      
      page.content = content
      page.plugin = plugin
    if page.save
      render :json => page
    end
  end

  def update
    page = Page.find(params[:id])
      page.title = params[:title]
      page.url = params[:url]
      page.content = params[:content]
      page.plugin = params[:plugin]
    if page.save
      render :json => page
    end
  end

  def delete
    page = Page.find(params[:id])
    if page.delete
      render :json => page
    else
    end
  end

  def checkurl

    map = Map.where({'url' => File.join('/',params[:url])})

    if map.empty?
      render :json => {:status => false}
    else
      render :json => {:status => true}
    end

  end

end
