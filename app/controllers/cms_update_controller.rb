class CmsUpdateController < ApplicationController

  layout "layouts/cms"
  
  before_filter :authenticate_user!
  authorize_resource :class => CmsUpdateController
#load_and_authorize_resource :class => CmsUpdateController

  def index
  end

  def show
    if(params[:id])
      render :json => Update.find(params[:id])
    else
      render :json => Update.all
    end
  end

  def save
    title = params[:title]
    url = params[:url]
    content = params[:content]
    enabled = params[:enabled]
    update = Update.new
      update.title = title
      update.url = url
      update.content = content
      update.enabled = enabled
    if update.save
      render :json => update
    end
  end

  def update
    payload = params[:cms_update][:_json]
    if payload.count > 1
      i=0
      payload.each do |up|
       item = Update.find(up.id)
       item.order = i
       i = i+1
      end
      @op = payload
    else
      #render :inline => 'one'
      @op = payload
    end
  end

  def delete
    update = Update.find(params[:id])
    if update.delete
      render :json => update
    else
    end
  end


end
