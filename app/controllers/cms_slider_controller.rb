class CmsSliderController < ApplicationController
  
  layout "layouts/cms"

  before_filter :authenticate_user!
  authorize_resource :class => CmsSliderController

  def index
  end

  def save
    slider = Slider.new
    slider.name = params[:modelName]
    slider.image = params[:myFile]
    slider.save

  slider = Slider.last
  slider.image_path = slider.image.url(:thumbnail)
    
    if slider.save
      flash[:notice] = "Image Uploaded for #{slider.image_path}"
      render :json => slider
    end

  end

  def show
      slider = Slider.all
      render :json => slider
  end

  def update

  end

  def delete

    slider = Slider.find(params[:id])

    slider.image.destroy
    slider.save

    slider = Slider.find(params[:id])

    slider.delete

  end

  def file_upload
  end
end
