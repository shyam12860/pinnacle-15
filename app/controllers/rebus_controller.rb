class RebusController < ApplicationController

	before_filter :authenticate_user!
	
	layout "layouts/rebus"

	def index
	end

	def game
		if user_signed_in?
			check = Rebus.where(pinnacleid: current_user.profile.pinnacleid).first
			if check
				@question = Rebusqna.where(round: check.roundno, id: check.questionno).first
			else
				game = Rebus.new
				game.pinnacleid = current_user.profile.pinnacleid
				game.roundno = 1
				game.questionno = 1
				game.trackprogress = 0
				game.save
				@question = Rebusqna.where(round: game.roundno, id: game.questionno).first
			end
		end
	end

	def skip_question
		if user_signed_in?
			check = Rebus.where(pinnacleid: current_user.profile.pinnacleid).first
			@details = Rebus.where(pinnacleid: current_user.profile.pinnacleid).first
			if check
				if check.questionno%7 == 0
					check.roundno += 1
					check.questionno += 1
				else
					check.questionno += 1
				end

				check.trackprogress += 0

				if check.save
					flash[:notice] = "Skipped!!! Looks like the last one is hard."
				end	
			end
		end

		redirect_to root_url + 'rebus/game'
	end

	def check_answer
		if user_signed_in?
			check = Rebus.where(pinnacleid: current_user.profile.pinnacleid).first
			@details = Rebus.where(pinnacleid: current_user.profile.pinnacleid).first
			if request.post?
				answer = params[:answer].downcase
				game = Rebusqna.where(round: check.roundno, id: check.questionno).first


				space_answer = answer.gsub(/\s+/, "")
				space_game = game.answer.gsub(/\s+/, "")

				spare_answer = answer.split(/\W+/)
				spare_game = game.answer.split(/\W+/)

				if space_answer == space_game
					if check.questionno%7 == 0
						check.roundno += 1
						check.questionno += 1
					else
						check.questionno += 1
					end

					check.trackprogress += 1
					if check.save
						flash[:notice] = "Last one went good, Try this."
					end		
				else
					intersection = spare_game & spare_answer

					percentile = ((intersection.size.to_f / spare_game.count.to_i) * 100)

					puts "----------------------------------------------------------------"
					puts spare_answer
					puts "******"
					puts spare_game
					puts "******"
					puts intersection.size
					puts "******"
					puts spare_game.count
					puts "******"
					puts percentile
					puts "----------------------------------------------------------------"

					if percentile < 25
						flash[:notice] = "The answer looks very incorrect  <br> <small><b>HINTS:</b> " + game.close_answer + "</small>"
					elsif percentile >= 25 and percentile <= 50
						flash[:notice] = "Good Start, we expect a lot from you  <br> <small><b>HINTS:</b> " + game.close_answer + "</small>"
					elsif percentile >= 51 and percentile <= 75
						flash[:notice] = "You have a half-baked answer, see if you can put it together <br> <small><b>HINTS:</b> " + game.close_answer + "</small>"
					elsif percentile >= 76 and percentile <= 99
						flash[:notice] = "You almost got it, a little effort <br> <small><b>HINTS:</b> " + game.close_answer + "</small>"
					elsif percentile == 100
						if check.questionno%7 == 0
							check.roundno += 1
							check.questionno += 1
						else
							check.questionno += 1
						end

						check.trackprogress += 1
						if check.save
							flash[:notice] = "Last one went good, Try this."
						end						
					end
				end

				
			end
		end

		redirect_to root_url + 'rebus/game'
	end

	def leaderboard
		@leaders = Rebus.order('trackprogress ASC, updated_at DESC').first(10)
	end

	def test_rebus
		@test = "Top Secret Only".downcase
		@test2 = "Top Secret".downcase

		@word = @test.split(/\W+/)
		@word2 = @test2.split(/\W+/)

		@intersection = @word & @word2

		@percentage =  ((@intersection.size.to_f/@word.count.to_i) * 100)

	end


end
