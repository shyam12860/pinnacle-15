var app = angular.module('cms',['textAngular']);

app.controller('mainController',['$scope','$http','$filter',function mainController($scope,$http,$filter){

	$scope.EventShow = false;
	$scope.WorkshopShow = false;
	$scope.DropdownChoice = 'all'
	$scope.SelectedWorkshop = 'none'
	$scope.SelectedEvent = 'none'

	$scope.DropdownSelect = function DropdownSelect()
	{

		if($scope.DropdownChoice == 'all')
		{
			$scope.EventShow = false;
			$scope.WorkshopShow = false;
		}
		else if($scope.DropdownChoice == 'events')
		{
			$scope.WorkshopShow = false;
			$scope.EventShow = true;
			$scope.SelectedWorkshop = 'none'
		}
		else if($scope.DropdownChoice == 'workshops')
		{
			$scope.EventShow = false;
			$scope.WorkshopShow = true;
     		$scope.SelectedEvent = 'none'

		}

	}



}]);

	