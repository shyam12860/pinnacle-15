// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap.min
//= require mousetrap.min
//= require jquery.slides.min
//= require jquery.vticker.min
//= require prefixfree.min
//= require angular
//= require angular-route.min
//= require angular-sanitize.min
//= require snap.svg-min

(function() {

        function init() {
          var speed = 250,
            easing = mina.easeinout;

          [].slice.call ( document.querySelectorAll( '#grid > div > a' ) ).forEach( function( el ) {
            var s = Snap( el.querySelector( 'svg' ) ), path = s.select( 'path' ),
              pathConfig = {
                from : path.attr( 'd' ),
                to : el.getAttribute( 'data-path-hover' )
              };

            el.addEventListener( 'mouseenter', function() {
              path.animate( { 'path' : pathConfig.to }, speed, easing );
            } );

            el.addEventListener( 'mouseleave', function() {
              path.animate( { 'path' : pathConfig.from }, speed, easing );
            } );
          } );
        }

        init();

      })();

$(function(){



  $(document).on('click','#loginTab', function(){
    console.log("1");
    $('#loginTabForm').show();
    $('#registerTabForm').hide();
  });

  $(document).on('click','#registerTab', function(){
    console.log("2");
    $('#registerTabForm').show();
    $('#loginTabForm').hide();
  });

});


var app = angular.module('main',['ngRoute','ngSanitize']);

app.config(['$routeProvider','$locationProvider',function($routeProvider, $locationProvider) {

}]);

// app.run(['$rootScope','$http',function($rootScope,$http) {
//     console.log("co");
//     $http({method: 'GET' , url: '/main/menu'}).success(function(data) {
//       $rootScope.mainnav = data;
//       console.log("C " + $rootScope.mainnav);
//     });
// }]);

app.controller('MenuCntl',['$rootScope','$scope','$http','$filter','$location', function ($rootScope,$scope,$http,$filter,$location) {
  $scope.subShow = true;
  // $scope.loadingText = true;

  // $scope.$on('$viewContentLoaded', function() { console.log("co"); });

  $http({method: 'GET' , url: '/main/menu'}).success(function(data) {
      $scope.subShow = false;
      $scope.secshow = false;
      $scope.loadingText = false;
      $scope.mainnav = data;
      $scope.opLevel2(0,1,'');
      //console.log(data);
  });



  $scope.opLevel2 = function opLevel2(id,index,url) {
    console.log($scope.mainnav[index].url)
    //$scope.subshow = false;

     if (url != ''){

            window.location.href = url;

            console.log('should redirect')
            return
     }
    $scope.level2fil = id;
    $scope.secshow = false;
    $('.nav-submenu').addClass('addHeightMenu');
    $('.nav-submenu').animate({marginTop: -15 },250,function(){
         //$(this).addClass('addHeightMenu');
         $scope.subShow = true;
         //console.log('dog')

        $(this).animate({marginTop: 0},250,function(){
        // $(this).addClass('addZIndexMenu');
        $(this).removeClass('addHeightMenu');
        $scope.subShow = true;

        });
    });
  }

  $scope.opLevel3 = function opLevel3(id,index,url) {

    if (url != ''){

            window.location.href = url;
            console.log('should redirect')
            return
     }
    console.log(id)
    $scope.level3fil = id;
    $scope.secshow = true;
  }




}]);

app.controller('ProfileCntl', ['$scope','$http', '$timeout', function($scope,$http,$timeout){

  $scope.showinst = false;
  $scope.showbra = false;
  $scope.showcor = false;

  $http({method: 'GET' , url: '/main/getprofile'}).
    success(function(data){
      $scope.profile = data
      console.log($scope.profile)
    }).
    error(function(data){
      console.log('error')
    });


  $http({method: 'GET' , url: '/assets/college.json'}).
    success(function(data){
      $scope.colleges = data
    }).
    error(function(data){
      console.log('error')
    });

    $http({method: 'GET' , url: '/assets/branch.json'}).
    success(function(data){
      $scope.branches = data
    }).
    error(function(data){
      console.log('error')
    });

    $http({method: 'GET' , url: '/assets/degree.json'}).
    success(function(data){
      $scope.courses = data
    }).
    error(function(data){
      console.log('error')
    });



  $scope.submitProfile = function submitProfile(){
    $http({method: 'POST', url: '/main/saveprofile' , data: $scope.profile}).
      success(function(data, status, headers, config) {

        console.log(data);
        $scope.profilemessage = data;

        if(data.status == "Success") {
          window.location.href = $('#referer').val();
        }
        console.log(data);
      }).
      error(function(data, status, headers, config) {
        //console.log(status)
      });
  };

}]);

app.controller('RegistrationCntl', ['$scope','$http', '$timeout', function($scope,$http,$timeout){

   $scope.regShow = true;
  $scope.regDisable = false;

  $scope.regdata = {}

  $scope.regdata.item_id = $('#regitem_id').val();
  $scope.regdata.category = $('#regcategory').val();

  $scope.submitRegistration = function submitRegistration(){
    $('#registerbutton').button('loading');
    $scope.regDisable = true;
    $http({method: 'POST', url: '/main/getregistration' , data: $scope.regdata}).
      success(function(data, status, headers, config) {
        $scope.regShow = false;
        $scope.regDisable = true;

        $scope.regMessage = data;

        console.log(data);


        $("#registerButton").attr('disabled',"disabled");
        console.log(data);

      }).
      error(function(data, status, headers, config) {
        //console.log(status)
        $scope.regDisable = false;
      });
      $('#registerbutton').button('reset');

  };

}]);

app.controller('HomeCntl', ['$scope', '$http', '$timeout',
  function ($scope, $http, $timeout) {

  	$http({method: 'GET' , url: '/main/updates'}).success(function(data) {
      $scope.updates = data;
    });


     $scope.slides = [];

   $http({method: 'GET' , url: '/main/slides'}).success(function(data) {
      $scope.slides = data;
    });

   $http({method: 'GET' , url: '/main/sponsors'}).success(function(data) {
      $scope.sponsors = data;
    });
}]);


// app.controller('MainController', ['$scope', '$http', '$routeParams', '$location', function ($scope, $route, $routeParams, $location) {
// 	$scope.$route = $route;
// 	$scope.$location = $location;
// 	$scope.$routeParams = $routeParams;
// }]);

app.directive('mySlides', function () {
    var directive = {
        restrict: 'A',
        link: function (scope, element, attrs, ctrl) {
            scope.$watch(attrs.mySlides, function (value) {
                setTimeout(function () {
                    // only if we have images since .slidesjs() can't
                    // be called more than once
                    console.log("attrs.start is:");
                    console.dir(attrs.start);
                    if (value.length > 0) {
                        $(element[0]).slidesjs({
                            preload: true,
                            play: {
                              active: true,
                              auto: true,
                              interval: 4000,
                              swap: true,
                              pauseOnHover: true,
                              restartDelay: 2500
                            }
                        });
                    }
                }, 1);
            });
        }
    };
    return directive;
});

app.directive('vTicker', ['$timeout', function($timeout) {
    var directive = {
      restrict: 'A',
      link: function($scope, iElm, iAttrs, controller) {
        $scope.$watch("updates", function(newValue,oldValue) {
          if(newValue && newValue.length > 0) {
            $timeout(function () {
              $(iElm).vTicker($scope.$eval(iAttrs.vTicker));
            }, 0);
          }
        })
      }
    };
    return directive;
}]);
// app.controller('updateController',['$scope','$http','$filter',function updateController($scope,$http,$filter){
// 	$scope.updates = {}

// 	$http({method: 'GET' , url: '/cms/updates'}).
// 		success(function(data){
// 			$scope.updates = data
// 			$scope.updateLoadComplete = true
// 			console.log(data)
// 		}).
// 		error(function(data){
// 			console.log('Error')
// 			$scope.updateLoadComplete = false
// 		});
// }]);

$(function() {

	$('[name="pinnacle-slider"]').slidesjs({
		navigation: false,
		play: {
          active: true,
          auto: true,
          interval: 4000,
          swap: true,
          pauseOnHover: true,
          restartDelay: 2500
        }
	});

	$('[name="pinnacle-ticker"]').vTicker();
});

app.directive('mySlides', function () {
    var directive = {
        restrict: 'A',
        link: function (scope, element, attrs, ctrl) {
            scope.$watch(attrs.mySlides, function (value) {
                setTimeout(function () {
                    // only if we have images since .slidesjs() can't
                    // be called more than once
                    console.log("attrs.start is:");
                    console.dir(attrs.start);
                    if (value.length > 0) {
                        $(element[0]).slidesjs({
                            preload: true,
                            play: {
                    active: true,
                    auto: true,
                    interval: 4000,
                    swap: true,
                    pauseOnHover: true,
                    restartDelay: 2500
                  },
                            pause: attrs.pause || 2500,
                            start: attrs.start || 1,
                            hoverPause: attrs.hoverPause || true,
                            navigation: {
                                active: true,
                                effect: "slide"
                            }
                        });
                    }
                }, 1);
            });
        }
    };
    return directive;
});
