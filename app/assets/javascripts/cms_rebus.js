//= require ckeditor/ckeditor

var app = angular.module('cms',[]);

app.controller('rebusController',['$scope','$http','$filter',function rebusController($scope,$http,$filter){

	$scope.rounds = []

	$http({method: 'GET' , url: '/cms/rebuss'}).
	success(function(data){
		$scope.rounds = data
		console.log($scope.rounds)
	}).
	error(function(data){
		console.log('error')
	});

	$scope.newRound = function newRound(){
		$http({method: 'POST', url: '/cms/rebus'}).
		success(function(data){

			$scope.rounds.push(data)
			console.log($scope.rounds)

		}).
		error(function(data){
			console.log('New Round Error')
		})

	}

	$scope.updateQ = function updateQ(r_index,q_index){

		$scope.currentQ = $scope.rounds[r_index][q_index];


		$http({method: 'PUT', url: '/cms/rebus', data: $scope.currentQ}).
		success(function(data){

			console.log(data)
		}).
		error(function(data){
			console.log('Update Error')
		})




	}


}]);

app.directive('ckEditor', function() {
  return {
    require: '?ngModel',
    link: function(scope, elm, attr, ngModel) {
      var ck = CKEDITOR.replace(elm[0]);

      if (!ngModel) return;

      ck.on('instanceReady', function() {
        ck.setData(ngModel.$viewValue);
      });

      function updateModel() {
          scope.$apply(function() {
              ngModel.$setViewValue(ck.getData());
          });
      }
        
      ck.on('change', updateModel);
      ck.on('key', updateModel);
      ck.on('dataReady', updateModel);

      ngModel.$render = function(value) {
        ck.setData(ngModel.$viewValue);
      };
    }
  };
});