require 'test_helper'

class CmsMainControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get dash" do
    get :dash
    assert_response :success
  end

end
