class CreateWorkshops < ActiveRecord::Migration
  def change
    create_table :workshops do |t|
      t.string :name
      t.text :description
      t.string :venue
      t.string :date
      t.string :time
      t.string :contact

      t.timestamps
    end
  end
end
