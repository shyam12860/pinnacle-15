class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.string :venue
      t.date :start_date
      t.date :end_date
      t.boolean :registration_required
      t.text :description

      t.timestamps
    end
  end
end
