class AddTabsToWorkshops < ActiveRecord::Migration
  def change
  	add_column :workshops, :tabs, :text
  	add_column :workshops, :url, :string
  	add_column :workshops, :meta, :string
  	remove_column :workshops, :date
  	remove_column :workshops, :time

  	add_column :workshops, :start_date, :date
  	add_column :workshops, :end_date, :date
  	add_column :workshops, :time, :time


  end
end
