class AddReferralIdToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :referralid, :string
  end
end
