class CreateRebusqnas < ActiveRecord::Migration
  def change
    create_table :rebusqnas do |t|
      t.integer :round
      t.text :question
      t.text :answer
      t.text :close_answer

      t.timestamps
    end
  end
end
