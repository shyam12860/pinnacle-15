class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.text :content
      t.string :title
      t.string :url
      t.string :plugin

      t.timestamps
    end
    add_index :pages, :url, :unique => true

  end
end
