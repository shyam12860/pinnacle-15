class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :pinnacleid, :uniq => true
      t.string :name
      t.string :gender
      t.string :course
      t.string :branch
      t.string :year
      t.string :institution
      t.string :phonenumber
      t.string :profilecomplete
      t.string :user_id, :uniq => true

      t.timestamps
    end
    
    add_index :profiles,:pinnacleid
    add_index :profiles,:user_id
  end
end
