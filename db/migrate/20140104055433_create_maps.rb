class CreateMaps < ActiveRecord::Migration
  def change
    create_table :maps do |t|
      t.string :type
      t.integer :item_id
      t.string :url

      t.timestamps
    end
  end
end
