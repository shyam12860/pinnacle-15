class CreateRegistrations < ActiveRecord::Migration
  def change
    create_table :registrations do |t|
      t.integer :user_id
      t.integer :item_id
      t.string :category
      t.text :details

      t.timestamps
    end
  end
end
