class AddNewFieldsToEvents < ActiveRecord::Migration
  def change
    add_column :events, :url, :string
    add_column :events, :tabs, :text
    add_column :events, :meta, :text
  end
end
