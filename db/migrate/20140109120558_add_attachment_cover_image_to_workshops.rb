class AddAttachmentCoverImageToWorkshops < ActiveRecord::Migration
  def self.up
    change_table :workshops do |t|
      t.attachment :cover_image
    end
  end

  def self.down
    drop_attached_file :workshops, :cover_image
  end
end
