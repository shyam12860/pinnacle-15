class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :primary
      t.string :secondary

      t.timestamps
    end
  end
end
