class AddTrackprogressToRebuses < ActiveRecord::Migration
  def change
    add_column :rebuses, :trackprogress, :integer
  end
end
