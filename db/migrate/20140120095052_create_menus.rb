class CreateMenus < ActiveRecord::Migration
  def change
    create_table :menus do |t|
      t.string :title
      t.text :url
      t.string :category
      t.integer :parent_id
      t.string :level

      t.timestamps
    end
  end
end
