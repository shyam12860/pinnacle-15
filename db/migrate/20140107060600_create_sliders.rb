class CreateSliders < ActiveRecord::Migration
  def change
    create_table :sliders do |t|
      t.string :name
      t.string :image_path
      t.string :image_file_name

      t.timestamps
    end
  end

  # def image_file_name
 	# image
  # end

  # def image_file_name(file_name)
 	# image = file_name
  # end
end
