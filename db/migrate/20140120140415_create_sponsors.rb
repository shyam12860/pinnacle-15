class CreateSponsors < ActiveRecord::Migration
  def change
    create_table :sponsors do |t|
      t.string :title
      t.string :url
      t.string :year
      t.string :category
      t.string :image_path

      t.timestamps
    end
  end
end
