class RenameTypetocategoryInMaps < ActiveRecord::Migration
  def change
  	rename_column :maps, :type, :category
  	add_index :maps, :url, :unique => true
  end
end
