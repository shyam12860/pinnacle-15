class CreateUpdates < ActiveRecord::Migration
  def change
    create_table :updates do |t|
      t.string :content
      t.string :url
      t.boolean :enabled
      t.string :order
      t.string :title

      t.timestamps
    end
  end
end
