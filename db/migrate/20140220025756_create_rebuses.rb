class CreateRebuses < ActiveRecord::Migration
  def change
    create_table :rebuses do |t|
      t.string :pinnacleid
      t.integer :roundno
      t.integer :questionno

      t.timestamps
    end
  end
end
