# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140226164146) do

  create_table "categories", force: true do |t|
    t.string   "primary"
    t.string   "secondary"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "url"
  end

  create_table "ckeditor_assets", force: true do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable"
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type"

  create_table "courses", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "events", force: true do |t|
    t.string   "name"
    t.string   "venue"
    t.date     "start_date"
    t.date     "end_date"
    t.boolean  "registration_required"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "url"
    t.text     "tabs"
    t.text     "meta"
    t.integer  "category_id"
    t.string   "cover_image_file_name"
    t.string   "cover_image_content_type"
    t.integer  "cover_image_file_size"
    t.datetime "cover_image_updated_at"
    t.integer  "user_id"
  end

  create_table "maps", force: true do |t|
    t.string   "category"
    t.integer  "item_id"
    t.string   "url"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "maps", ["url"], name: "index_maps_on_url", unique: true

  create_table "menus", force: true do |t|
    t.string   "title"
    t.text     "url"
    t.string   "category"
    t.integer  "parent_id"
    t.string   "level"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "order"
  end

  create_table "pages", force: true do |t|
    t.text     "content"
    t.string   "title"
    t.string   "url"
    t.string   "plugin"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "pages", ["url"], name: "index_pages_on_url", unique: true

  create_table "profiles", force: true do |t|
    t.string   "pinnacleid"
    t.string   "name"
    t.string   "gender"
    t.string   "course"
    t.string   "branch"
    t.string   "year"
    t.string   "institution"
    t.string   "phonenumber"
    t.string   "profilecomplete"
    t.string   "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "piamb"
    t.string   "referralid"
    t.string   "how"
  end

  add_index "profiles", ["pinnacleid"], name: "index_profiles_on_pinnacleid"
  add_index "profiles", ["user_id"], name: "index_profiles_on_user_id"

  create_table "rebuses", force: true do |t|
    t.string   "pinnacleid"
    t.integer  "roundno"
    t.integer  "questionno"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "trackprogress"
  end

  create_table "rebusqnas", force: true do |t|
    t.integer  "round"
    t.text     "question"
    t.text     "answer"
    t.text     "close_answer"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "registrations", force: true do |t|
    t.integer  "user_id"
    t.integer  "item_id"
    t.string   "category"
    t.text     "details"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "payment"
  end

  create_table "sliders", force: true do |t|
    t.string   "name"
    t.string   "image_path"
    t.string   "image_file_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "snippets", force: true do |t|
    t.string   "name"
    t.string   "content"
    t.string   "category"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "snippets", ["name"], name: "index_snippets_on_name", unique: true

  create_table "sponsors", force: true do |t|
    t.string   "title"
    t.string   "url"
    t.string   "year"
    t.string   "category"
    t.string   "image_path"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "updates", force: true do |t|
    t.string   "content"
    t.string   "url"
    t.boolean  "enabled"
    t.string   "order"
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "uid"
    t.string   "provider"
    t.text     "oauth_token"
    t.string   "oauth_expires"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "role"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

  create_table "workshops", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "venue"
    t.string   "contact"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "tabs"
    t.string   "url"
    t.string   "meta"
    t.date     "start_date"
    t.date     "end_date"
    t.time     "time"
    t.string   "cover_image_file_name"
    t.string   "cover_image_content_type"
    t.integer  "cover_image_file_size"
    t.datetime "cover_image_updated_at"
    t.integer  "user_id"
  end

end
