-- MySQL dump 10.13  Distrib 5.5.34, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: sme
-- ------------------------------------------------------
-- Server version	5.5.34-0ubuntu0.12.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `primary` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `secondary` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'event','Online Events','2014-01-16 10:26:12','2014-01-16 10:26:12','/onlineevents'),(2,'event','Robotics','2014-01-16 10:26:38','2014-01-16 10:26:38','/robotics'),(3,'event','Mechmania','2014-01-16 10:26:59','2014-01-16 10:26:59','/mechmania'),(4,'event','Quiz','2014-01-16 10:27:12','2014-01-16 10:27:12','/quiz'),(6,'event','Gamindrome','2014-01-16 10:27:58','2014-01-16 10:27:58','/game'),(9,'workshop','Advanced Industrial Casting & Forming','2014-01-16 10:29:54','2014-01-16 10:29:54','/aicf'),(10,'workshop','Real-Time Interactive Modelling','2014-01-16 10:30:29','2014-01-16 10:30:29','/rtim'),(11,'event','Suprise Event','2014-01-16 12:30:31','2014-01-16 12:30:31','/supriseevent'),(12,'workshop','Auto-Anatomy','2014-01-16 12:30:49','2014-01-16 12:30:49','/aa');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ckeditor_assets`
--

DROP TABLE IF EXISTS `ckeditor_assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ckeditor_assets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_file_size` int(11) DEFAULT NULL,
  `assetable_id` int(11) DEFAULT NULL,
  `assetable_type` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_ckeditor_assetable_type` (`assetable_type`,`type`,`assetable_id`),
  KEY `idx_ckeditor_assetable` (`assetable_type`,`assetable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ckeditor_assets`
--

LOCK TABLES `ckeditor_assets` WRITE;
/*!40000 ALTER TABLE `ckeditor_assets` DISABLE KEYS */;
/*!40000 ALTER TABLE `ckeditor_assets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `venue` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `registration_required` tinyint(1) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tabs` text COLLATE utf8_unicode_ci,
  `meta` text COLLATE utf8_unicode_ci,
  `category_id` int(11) DEFAULT NULL,
  `cover_image_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cover_image_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cover_image_file_size` int(11) DEFAULT NULL,
  `cover_image_updated_at` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (1,'GAMINDROME','Department of Mechanical Engineering',NULL,NULL,0,NULL,'2014-01-13 17:33:30','2014-01-17 15:08:00','events/gamindrome','[{\"title\":\"INTRODUCTION\",\"content\":\"\\u003Cdiv\\u003EDo you enjoy gaming and beating other players online?\\u0026nbsp;\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003EWhy not turn that skill and natural finger flair into serious money? All the gamers, ASSEMBLE!\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003EIts time to put those gaming skills to the ultimate test and this is the perfect platform to do so!\\u0026nbsp;Let the friskiest fingers win !!\\u003C/div\\u003E\"}]','null',999,'gamingdrome.png','image/png',612561,'2014-01-16 08:51:18',NULL),(2,'DEATHRACE','Department of Mechanical Engineering',NULL,NULL,1,NULL,'2014-01-13 17:33:56','2014-01-21 17:42:41','events/deathrace','[{\"title\":\"INTRODUCTION\",\"content\":\"\\u003Cblockquote\\u003E\\u003Cb\\u003E“FASTER! FASTER!! FASTER!!! Until the thrill of SPEED overcomes the fear of DEATH!!\\\"\\u003C/b\\u003E\\u003C/blockquote\\u003E\\u003Cdiv style=\\\"text-align: right;\\\"\\u003E-Hunter Thompson\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003ESo you really think your all terrain bot is up to the task?? We are here to prove you wrong!! The most scintillating event of Pinnacle is ready for the big show. It’s time to gear up your bot to race in our arduous track. You will find yourself in breath-taking moments, struggling to escape the jaws of death. With the best bots on the wheels and excited crowd on their heels, the most crucial experience awaits you.\\u003C/div\\u003E\"},{\"title\":\"PROBLEM STATEMENT\",\"content\":\"To be updated soon.\"},{\"title\":\"SPECIFICATIONS\",\"content\":\"To be updated soon.\"},{\"title\":\"GAME PROCEDURE\",\"content\":\"To be updated soon.\"},{\"title\":\"RULES \\u0026 SCORING\",\"content\":\"To be updated soon.\"},{\"title\":\"CONTACT\",\"content\":\"To be updated soon.\"}]','null',NULL,'deathrace.png','image/png',565925,'2014-01-16 08:52:17',NULL),(3,'REAL STEEL','Department of Mechanical Engineering',NULL,NULL,1,NULL,'2014-01-13 17:34:13','2014-01-18 15:54:08','events/realsteel','[{\"title\":\"INTRODUCTION\",\"content\":\"\\u003Cblockquote\\u003E\\u003Cb\\u003E\\\"THE SURVIVAL OF THE FITTEST AND THE SMARTEST”\\u003C/b\\u003E\\u003C/blockquote\\u003E\\u003Cdiv\\u003EThe machine has no feelings, it feels no pain or fear…It operates according to the pure logic of probability and perceives more accurately than man. Remember we are not the only avatars of humanity. Once our computing machines achieved self-consciousness, they became part of this design.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003EAre you bored of reading technical books? Want to show off your strong invention? Then you are on the right event, Sumo Wars.\\u0026nbsp;Make an unstoppable muscular robot to push the opponents out of the ring. Let the crowd feast on the clash of metals.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\"},{\"title\":\"PROBLEM STATEMENT\",\"content\":\"To be updated soon.\"},{\"title\":\"EVENT FORMAT\",\"content\":\"To be updated soon.\"},{\"title\":\"SPECIFICATIONS\",\"content\":\"To be updated soon.\"},{\"title\":\"RULES \\u0026 SCORING\",\"content\":\"To be updated soon.\"}]','null',NULL,'real_steel.png','image/png',565344,'2014-01-16 08:52:44',NULL),(4,'LINE TRACER','Department of Mechanical Engineering',NULL,NULL,1,NULL,'2014-01-13 17:34:26','2014-01-16 18:32:52','events/linetracer','[{\"title\":\"INTRODUCTION\",\"content\":\"Are you thinking of giving your bot its own brain to make its decision, instead of controlling it manually? Are you trying to give your bot a piece of your mind? Then, this is the event you are looking for. Your bot will only be guided by a black line; it’s up to your bot to decide the right path avoiding distractions to reach the haven.\"},{\"title\":\"PROBLEM STATEMENT\",\"content\":\"\\u003Cp class=\\\"MsoNormal\\\" style=\\\"margin-bottom: 16.2pt; line-height: 15.75pt; background-position: initial initial; background-repeat: initial initial;\\\"\\u003E\\u003Cfont color=\\\"#000000\\\" size=\\\"3\\\"\\u003ETo be updated soon.\\u003C/font\\u003E\\u003C/p\\u003E\"},{\"title\":\"EVENT FORMAT\",\"content\":\"\\u003Cspan style=\\\"color: rgb(0, 0, 0); font-size: medium; line-height: 21px;\\\"\\u003ETo be updated soon.\\u003C/span\\u003E\"},{\"title\":\"SCORING CRITERIA\",\"content\":\"\\u003Cspan style=\\\"color: rgb(0, 0, 0); font-size: medium; line-height: 21px;\\\"\\u003ETo be updated soon.\\u003C/span\\u003E\"},{\"title\":\"RULES \\u0026 SPECIFICATIONS\",\"content\":\"\\u003Cspan style=\\\"color: rgb(0, 0, 0); font-size: medium; line-height: 21px;\\\"\\u003ETo be updated soon.\\u003C/span\\u003E\"},{\"title\":\"CONTACT\",\"content\":\"\\u003Cspan style=\\\"color: rgb(0, 0, 0); font-size: medium; line-height: 21px;\\\"\\u003ETo be updated soon.\\u003C/span\\u003E\"}]','null',NULL,'line_tracer.png','image/png',475303,'2014-01-16 08:53:16',NULL),(5,'GOD SPEED','Mini-Gallery, University Sports Ground',NULL,NULL,1,NULL,'2014-01-13 17:35:00','2014-01-18 15:54:42','events/godspeed','[{\"title\":\"INTRODUCTION\",\"content\":\"\\u003Cblockquote\\u003E\\u003Cb\\u003EOnce something is a passion, the motivation is there.\\u003C/b\\u003E\\u003C/blockquote\\u003E\\u003Cdiv style=\\\"text-align: right;\\\"\\u003E- Michael Schumacher\\u003C/div\\u003E\\u003Cdiv\\u003EA test of passion. A test to endure competition. With twist and turns, bumps and bends, and of course, never seen before spectators blended with revving engines and drifting machines provide the perfect platform to showcase your articulation in engineering and mental power. Get set as GOD SPEED sparks off here at PINNACLE ‘14.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\"},{\"title\":\"PROBLEM STATEMENT\",\"content\":\"To be updated soon.\"},{\"title\":\"ABSTRACT SUBMISSION\",\"content\":\"To be updated soon.\"},{\"title\":\"EVENT FORMAT\",\"content\":\"To be updated soon.\"},{\"title\":\"SPECIFICATION\",\"content\":\"To be updated soon.\"},{\"title\":\"GENERAL RULES\",\"content\":\"To be updated soon.\"},{\"title\":\"RACE RULES\",\"content\":\"To be updated soon.\"},{\"title\":\"CONTACT US\",\"content\":\"To be updated soon.\"}]','null',NULL,'Godspeed.png','image/png',560592,'2014-01-16 08:54:35',NULL),(6,'OFFSHORE CRUISER','Center for Water Resources Lab, CEG Campus',NULL,NULL,1,NULL,'2014-01-13 17:36:02','2014-01-16 17:38:06','events/offshore','[{\"title\":\"INTRODUCTION\",\"content\":\"\\u003Cblockquote\\u003E\\u003Cb\\u003E\\\"Not everybody wins, and certainly not everybody wins all the time. But once you get into your boat and push off, tie into your shoes and boot stretchers then \\\"lean on the oars,\\\" you have indeed won far more than those who have never tried.\\\"\\u003C/b\\u003E\\u003C/blockquote\\u003E\\u003Cdiv\\u003ETired of seeing robots race on land all the time? Ever thought about being different from the others and making them race on water? Pinnacle 2014 brings to you the Robotic \'Boat Race\'- an innovative water racing event. Design your bots with the objective of completing the given water track ahead of your competitors. Put on your thinking caps and come up with the best design to win attractive prizes!\\u0026nbsp;\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\"}]','null',NULL,'cruiser.png','image/png',276049,'2014-01-16 08:56:27',NULL),(7,'INNOVATE','Department of Mechanical Engineering',NULL,NULL,0,NULL,'2014-01-13 17:36:13','2014-01-16 17:39:26','events/innovate','[{\"title\":\"INTRODUCTION\",\"content\":\"\\u003Cdiv\\u003ERigorous preparation is for those who don’t know how to handle things instantaneously!! Why sit and prepare when you already have been doing it for all these years?? Enough of preparing, let’s start acting!!\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cblockquote\\u003E\\u003Cb\\u003E“Don’t put me on the spot. I can’t give you an instant solution.”\\u0026nbsp;\\u003C/b\\u003E\\u003C/blockquote\\u003E\\u003Cdiv\\u003EWell that’s for the weak and not for the others.God is not the only creator!! We engineers can create too!!\\u003C/div\\u003E\\u003Cdiv\\u003EEver wanted to create something and bask in its glory?? Well here is your chance to build something right from the scratch. Think promptly and act rapidly!!\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\"}]','null',NULL,'innovte.png','image/png',547239,'2014-01-16 08:57:10',NULL),(8,'PAPER PRESENTATION','Department of Mechanical Engineering',NULL,NULL,1,NULL,'2014-01-13 17:36:39','2014-01-17 17:20:32','events/paperpresentation','[{\"title\":\"INTRODUCTION\",\"content\":\"\\u003Cblockquote\\u003E\\u003Cb\\u003E“Imagination is everything. It is the preview of life\'s coming attractions.\\\"\\u003C/b\\u003E\\u003C/blockquote\\u003E\\u003Cp style=\\\"text-align: right;\\\"\\u003E-\\u003Cspan class=\\\"Apple-tab-span\\\" style=\\\"white-space:pre\\\"\\u003E\\t\\u003C/span\\u003EAlbert Einstein\\u003C/p\\u003E\\u003Cp\\u003EDo you have an idea, and can’t wait to share it with the world? Do you have a possible solution to the current global demands and ever increasing challenges of the present?\\u003C/p\\u003E\\u003Cp\\u003EThen here, at Pinnacle, we’ll give you the stage and opportunity to showcase what you have got. Every innovation is born from an idea. Pen those ideas down in the paper, present them in front of our judges, impress our experts, and walk away with honors!\\u003C/p\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\"},{\"title\":\"EVENT FORMAT\",\"content\":\"To be updated soon.\"},{\"title\":\"RULES\",\"content\":\"To be updated soon.\"},{\"title\":\"CONTACT\",\"content\":\"To be updated soon.\"}]','null',NULL,'paper_presentation.png','image/png',500473,'2014-01-16 08:57:46',NULL),(9,'CONTRAPTIONS','Department of Mechanical Engineering',NULL,NULL,1,NULL,'2014-01-13 17:37:00','2014-01-17 17:20:55','events/contraptions','[{\"title\":\"INTRODUCTION\",\"content\":\"\\u003Cdiv\\u003E\\u003Cblockquote\\u003E\\u003Cb\\u003E\\\"The winner is the chef who takes the same ingredients as everyone else and produces the best results.\\\"\\u003C/b\\u003E\\u003C/blockquote\\u003E\\u003Cdiv style=\\\"text-align: right;\\\"\\u003E-\\u003Cspan class=\\\"Apple-tab-span\\\" style=\\\"white-space:pre\\\"\\u003E\\t\\u003C/span\\u003EEdward de Bono\\u003C/div\\u003E\\u003Cdiv\\u003EPinnacle 2014 welcomes you to showcase your talents and gives you the license to display the most chaotic, complex and convoluted means to the end.\\u0026nbsp;Throw in all the energy conversions you are aware of as one continuous routine, without physically engaging yourself at any stage.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003ESo, set your grey cells to the task, let your imagination run wild, let creativity take the lead and let your approach be totally unconventional.\\u003C/div\\u003E\\u003Cdiv\\u003EReady, set, go!\\u003C/div\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\"},{\"title\":\"PROBLEM STATEMENT\",\"content\":\"To be updated soon.\"},{\"title\":\"MODULES\",\"content\":\"To be updated soon.\"},{\"title\":\"EVALUATION\",\"content\":\"To be updated soon.\"},{\"title\":\"RULES\",\"content\":\"To be updated soon.\"},{\"title\":\"FAQs\",\"content\":\"To be updated soon.\"},{\"title\":\"CONTACT\",\"content\":\"To be updated soon.\"}]','null',NULL,'contraptions.png','image/png',585791,'2014-01-16 08:58:23',NULL),(10,'TYRE-A-THON','Department of Mechanical Engineering',NULL,NULL,0,NULL,'2014-01-13 17:37:28','2014-01-19 13:08:27','events/tyreathon','[{\"title\":\"INTRODUCTION\",\"content\":\"Are you amazed by the speed of the pit crew in the Grand Prix? Ever fancy yourselves in their place?\\u0026nbsp;\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003EWelcome to the Tyre Change Event where speed and teamwork is all that matters. Remove a tyre from a particular automobile and replace it in the shortest possible time! Get ready for some pure muscle power and fun!!\\u003C/div\\u003E\"}]','null',NULL,'tyre_change_event.png','image/png',460574,'2014-01-16 08:59:28',NULL),(11,'I-MOTO','Department of Mechanical Engineering',NULL,NULL,0,NULL,'2014-01-13 17:38:24','2014-01-19 06:29:29','events/imotoauto','[{\"title\":\"INTRODUCTION\",\"content\":\"Are you crazy about automobiles? Have you always been dreaming about working with one of the biggest automobile companies in India?\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003EThen here\'s your chance to get the most from one of the giants in the auto industry. Take part in the Ford sponsored Company Quiz and battle it out for the ultimate prize- an assured internship opportunity at FORD itself!\\u003C/div\\u003E\"}]','null',NULL,'auto_quiz.png','image/png',465685,'2014-01-16 09:08:13',NULL),(12,'BRAIN BUZZ','Department of Mechanical Engineering',NULL,NULL,0,NULL,'2014-01-13 17:38:34','2014-01-16 17:47:55','events/brainbuzz','[{\"title\":\"INTRODUCTION\",\"content\":\"\\u003Cblockquote\\u003E\\u003Cb\\u003E\\\"Nothing will ever be attempted if all possible objections must first be overcome.\\\"\\u0026nbsp;\\u003C/b\\u003E\\u003C/blockquote\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003EBored of engineering?? Well you have come to the right place!! A game to check your knowledge in business, it’s insights and it’s related concepts.\\u0026nbsp;\\u003C/div\\u003E\\u003Cdiv\\u003EBudding entrepreneurs from all over India will be competing in this quiz. Potential angel investors and venture capitalists will emerge here. Mark your calendar for this one!\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\"}]','null',NULL,'biz_quiz.png','image/png',542200,'2014-01-16 09:11:15',NULL),(13,'GENERAL QUIZ','Department of Mechanical Engineering',NULL,NULL,0,NULL,'2014-01-13 17:38:48','2014-01-16 17:49:38','events/generalquiz','[{\"title\":\"INTRODUCTION\",\"content\":\"Who says technical knowledge is everything?? It’s just one thing!! For all those who believe in this, here is your chance to prove that\\u0026nbsp;\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003Cblockquote\\u003E\\u003Cb\\u003E“Jack of all trades is in fact better than the master of one“.\\u0026nbsp;\\u003C/b\\u003E\\u003C/blockquote\\u003E\\u003Cdiv\\u003EGet ready for a wide variety of questions in every possible field that one can think of!! Rack your brains hard for “Even if you come second, you\'re just the first loser!”\\u003C/div\\u003E\\u003C/div\\u003E\"}]','null',NULL,'general_quiz.png','image/png',417216,'2014-01-16 09:09:48',NULL),(14,'BRAIN TEASER','Department of Mechanical Engineering',NULL,NULL,0,NULL,'2014-01-13 17:39:06','2014-01-16 17:50:45','events/brainteaser','[{\"title\":\"INTRODUCTION\",\"content\":\"\\u003Cdiv\\u003EThe mind is where the universe resides. It is the genesis of thought. A precious thought that can change the world! Are you brainy enough? Are you good at Deductions, logical, lateral thinking and identifying? Then get ready to be delighted, this PINNACLE 2014.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003EWe promise to enthrall you, with never seen before puzzles, ranging from the simple and the obvious to the nail-biting brain teasing puzzles. This event provides you with uninterrupted intellectual entertainment. This event does not require any prior engineering knowledge. All you need is the enthusiasm required to solve puzzles and you’ll get a chance to grab some exciting prizes.\\u003C/div\\u003E\"}]','null',NULL,'brainteaser.png','image/png',467681,'2014-01-16 09:12:02',NULL),(15,'AUTOMOTIVE PHOTOGRAPHY','Department of Mechanical Engineering',NULL,NULL,1,NULL,'2014-01-13 17:39:26','2014-01-16 17:52:01','events/autophotography','[{\"title\":\"INTRODUCTION\",\"content\":\"\\u003Cblockquote\\u003E\\u003Cb\\u003E“You don’t take a photograph, you make it.\\\"\\u003C/b\\u003E\\u003C/blockquote\\u003E\\u003Cdiv style=\\\"text-align: right;\\\"\\u003E\\u0026nbsp;- Ansel Adams\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003EFor all the Lensmen out there, the stage is yours.How often have you seen a photograph, thinking, “This is a good photograph, but there is something missing in it!” ? Sometimes small things make a big difference. Don’t be afraid to shake things up. Never stop photographing. It is very likely that your best photograph has not yet been captured. Your prize is just a click away!\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\"}]','null',NULL,'automotic_photography.png','image/png',401833,'2014-01-16 09:19:50',NULL),(16,'SURPRISE EVENTS','Department of Mechanical Engineering',NULL,NULL,0,NULL,'2014-01-13 17:39:45','2014-01-17 16:19:18','events/surprise','[{\"title\":\"INTRODUCTION\",\"content\":\"\\u003Cdiv\\u003EOh well….the name says it all!! \\u0026nbsp;Ever wanted to surprise yourself with a price??\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003EFor all those people who have become tired of preparing something for every symposium, this is your place!! No preparation needed!! Just walk in and play for fun to win exciting prizes and gift vouchers!!\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\"}]','null',NULL,NULL,NULL,NULL,NULL,NULL),(18,'P! AMBASSADOR','Department of Mechanical Engineering',NULL,NULL,1,NULL,'2014-01-17 08:05:52','2014-01-20 16:11:11','/piambassador','[{\"title\":\"INTRODUCTION\",\"content\":\"\\u003Cdiv\\u003EHave you ever been the crowd puller in your campus? Are you the pivot for your peers or have you considered becoming one? Then, this is where you have to register.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003ESociety of Mechanical Engineers, Anna University presents the \\u003Cb\\u003EP! Ambassador Program \\u003C/b\\u003Efor PINNACLE 2014.\\u0026nbsp;We give you a chance to encourage your fellow Engineers to register and participate actively in this holistic platform for everyone graduating through Engineering studies, not just Mechanical Engineers. Turn heads towards one of the biggest departmental tech fests!\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003ECheck the benefits and requirements of this versatile-program.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\"},{\"title\":\"BENEFITS\",\"content\":\"\\u003Cdiv\\u003E\\u003Cdiv\\u003E\\u0026nbsp;For best Pi AMBASSADORS decided by the Marketing Committee of SME:-\\u003C/div\\u003E\\u003Cdiv\\u003E\\u0026nbsp;\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cul\\u003E\\u003Cli\\u003ET-Shirts and Bags\\u003C/li\\u003E\\u003Cli\\u003EFree onsite Registration\\u003C/li\\u003E\\u003Cli\\u003E\\u003Ci\\u003EPlacement- Recognized\\u003C/i\\u003E Official Letters of Appreciation for actively marketing PINNACLE 2014 from SME, Anna University,Ch-25\\u003C/li\\u003E\\u003Cli\\u003EInvaluable experience in marketing an Event/Workshop\\u003C/li\\u003E\\u003Cli\\u003EA reputed certificate of appreciation for actively marketing Pinnacle 2014.\\u003C/li\\u003E\\u003Cli\\u003ECash prizes worth Rs.5000!\\u003C/li\\u003E\\u003Cli\\u003ECash discounts for participants registering under your Pinnacle Ambassador ID (details of which will be sent by mail upon registration)\\u003C/li\\u003E\\u003Cli\\u003EPremium hospitality for the ambassadors during PINNACLE 2014 such as food coupons and accommodation if needed.\\u003C/li\\u003E\\u003C/ul\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Ci\\u003E\\u003Cb\\u003ENOTE:\\u003C/b\\u003E The final count of the number of registrations made online under an ambassador will be cross-checked at the hospitality desk to validate the onsite registrations.\\u003C/i\\u003E\\u003Cb\\u003E\\u003Cbr\\u003E\\u003C/b\\u003E\\u003C/div\\u003E\\u003C/div\\u003E\"},{\"title\":\"REQUIREMENTS\",\"content\":\"\\u003Cdiv\\u003E\\u003Cdiv\\u003EFor qualities of Pi AMBASSADORS which are looked by the Marketing Committee of SME\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cul\\u003E\\u003Cli\\u003ECollege UG/PG student pursuing a Bachelors in Engineering Degree in any year.\\u003C/li\\u003E\\u003Cli\\u003EGood inter-personal, General Marketing and extra-curricular skills.\\u003C/li\\u003E\\u003Cli\\u003EYour sincerity and dedication towards gathering as many people as possible to PINNACLE 2014.\\u003C/li\\u003E\\u003Cli\\u003EPassion for learning, creativity, innovation \\u0026amp; information sharing.\\u003C/li\\u003E\\u003Cli\\u003EActive response to Pinnacle Marketing Committee via Email, Facebook and Phone Calls.\\u003C/li\\u003E\\u003C/ul\\u003E\\u003C/div\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\"},{\"content\":\"\\u003Cdiv\\u003ELimited Rolling Registrations!\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003ERegistration for P! Ambassador Program will be updated soon.\",\"title\":\"REGISTRATION\"},{\"title\":\"CONTACT\",\"content\":\"\\u003Cdiv\\u003EE-Mail:\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cb\\u003E\\u003Cbr\\u003E\\u003C/b\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cb\\u003Emarketing@smeannauniv.org\\u003C/b\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cb\\u003E\\u003Cbr\\u003E\\u003C/b\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cb\\u003Eonlinemarketing@smeannauniv.org\\u003C/b\\u003E\\u003C/div\\u003E\"}]','null',999,'banner.png','image/png',63580,'2014-01-17 08:28:34',NULL);
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maps`
--

DROP TABLE IF EXISTS `maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_maps_on_url` (`url`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maps`
--

LOCK TABLES `maps` WRITE;
/*!40000 ALTER TABLE `maps` DISABLE KEYS */;
INSERT INTO `maps` VALUES (1,'workshop',1,'/workshops/autoanatomy','2014-01-13 17:13:14','2014-01-17 06:18:21'),(2,'workshop',2,'/workshops/adcasting','2014-01-13 17:22:58','2014-01-17 06:17:20'),(3,'workshop',3,'/workshops/augmentedreality','2014-01-13 17:25:28','2014-01-17 06:09:37'),(4,'event',1,'/events/gamindrome','2014-01-13 17:33:30','2014-01-17 06:21:17'),(5,'event',2,'/events/deathrace','2014-01-13 17:33:56','2014-01-17 12:38:03'),(6,'event',3,'/events/realsteel','2014-01-13 17:34:13','2014-01-17 06:21:52'),(7,'event',4,'/events/linetracer','2014-01-13 17:34:26','2014-01-17 06:18:36'),(8,'event',5,'/events/godspeed','2014-01-13 17:35:00','2014-01-17 06:14:29'),(9,'event',6,'/events/offshore','2014-01-13 17:36:02','2014-01-17 06:18:53'),(10,'event',7,'/events/innovate','2014-01-13 17:36:13','2014-01-17 06:18:56'),(11,'event',8,'/events/paperpresentation','2014-01-13 17:36:39','2014-01-17 06:19:00'),(12,'event',9,'/events/contraptions','2014-01-13 17:37:00','2014-01-17 06:19:06'),(13,'event',10,'/events/tyreathon','2014-01-13 17:37:28','2014-01-19 13:08:27'),(14,'event',11,'/events/imotoauto','2014-01-13 17:38:24','2014-01-17 06:19:12'),(15,'event',12,'/events/brainbuzz','2014-01-13 17:38:34','2014-01-17 06:22:16'),(16,'event',13,'/events/generalquiz','2014-01-13 17:38:48','2014-01-17 06:22:13'),(17,'event',14,'/events/brainteaser','2014-01-13 17:39:06','2014-01-17 06:22:09'),(18,'event',15,'/events/autophotography','2014-01-13 17:39:26','2014-01-17 06:22:06'),(19,'event',16,'/events/surprise','2014-01-13 17:39:45','2014-01-17 16:19:18'),(21,'category',1,'/onlineevents','2014-01-16 10:26:12','2014-01-16 10:26:12'),(22,'category',2,'/robotics','2014-01-16 10:26:38','2014-01-16 10:26:38'),(23,'category',3,'/mechmania','2014-01-16 10:26:59','2014-01-16 10:26:59'),(24,'category',4,'/quiz','2014-01-16 10:27:12','2014-01-16 10:27:12'),(26,'category',6,'/game','2014-01-16 10:27:58','2014-01-16 10:27:58'),(29,'category',9,'/aicf','2014-01-16 10:29:54','2014-01-16 10:29:54'),(30,'category',10,'/rtim','2014-01-16 10:30:29','2014-01-16 10:30:29'),(31,'category',11,'/supriseevent','2014-01-16 12:30:31','2014-01-16 12:30:31'),(32,'category',12,'/aa','2014-01-16 12:30:49','2014-01-16 12:30:49'),(33,'event',18,'/piambassador','2014-01-17 08:05:52','2014-01-17 08:05:52');
/*!40000 ALTER TABLE `maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text COLLATE utf8_unicode_ci,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plugin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_pages_on_url` (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profiles`
--

DROP TABLE IF EXISTS `profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pinnacleid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `course` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `branch` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `year` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `institution` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phonenumber` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profilecomplete` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_profiles_on_pinnacleid` (`pinnacleid`),
  KEY `index_profiles_on_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profiles`
--

LOCK TABLES `profiles` WRITE;
/*!40000 ALTER TABLE `profiles` DISABLE KEYS */;
INSERT INTO `profiles` VALUES (1,'PIN0001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','2014-01-13 14:55:25','2014-01-13 14:55:25'),(2,'PIN0002',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2','2014-01-13 20:16:55','2014-01-13 20:16:55'),(3,'PIN0003',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'3','2014-01-15 13:16:36','2014-01-15 13:16:36'),(4,'PIN0004',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'4','2014-01-15 13:19:25','2014-01-15 13:19:25'),(5,'PIN0005',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'5','2014-01-15 13:21:34','2014-01-15 13:21:34'),(6,'PIN0006',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'6','2014-01-15 14:05:39','2014-01-15 14:05:39'),(7,'PIN0007',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'7','2014-01-19 15:02:59','2014-01-19 15:02:59');
/*!40000 ALTER TABLE `profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_migrations`
--

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
INSERT INTO `schema_migrations` VALUES ('20131211161940'),('20131211162031'),('20131212135831'),('20131225162918'),('20131229141358'),('20140104055433'),('20140104060057'),('20140104060603'),('20140104062223'),('20140104062813'),('20140104063343'),('20140104184917'),('20140105112624'),('20140106144518'),('20140107060600'),('20140108145259'),('20140108145742'),('20140109020406'),('20140109120558'),('20140111062030'),('20140111120043'),('20140111120052'),('20140115080035');
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sliders`
--

DROP TABLE IF EXISTS `sliders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sliders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `image_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_file_size` int(11) DEFAULT NULL,
  `image_updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sliders`
--

LOCK TABLES `sliders` WRITE;
/*!40000 ALTER TABLE `sliders` DISABLE KEYS */;
INSERT INTO `sliders` VALUES (24,'','/slider/images/24_thumbnail.jpg?1389957891','2014-01-17 11:24:53','2014-01-17 11:24:53','dept_n_ceg.png','image/png',546069,'2014-01-17 11:24:51'),(25,'','/slider/images/25_thumbnail.jpg?1389957917','2014-01-17 11:25:19','2014-01-17 11:25:19','god_speed_big.png','image/png',533536,'2014-01-17 11:25:17'),(28,'','/slider/images/28_thumbnail.jpg?1390059900','2014-01-18 15:45:02','2014-01-18 15:45:02','ceg_colour.png','image/png',617110,'2014-01-18 15:45:00'),(29,'','/slider/images/29_thumbnail.jpg?1390059931','2014-01-18 15:45:33','2014-01-18 15:45:33','workshops.png','image/png',594062,'2014-01-18 15:45:31'),(30,'','/slider/images/30_thumbnail.jpg?1390059977','2014-01-18 15:46:19','2014-01-18 15:46:19','robotics_big.png','image/png',558480,'2014-01-18 15:46:17');
/*!40000 ALTER TABLE `sliders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `snippets`
--

DROP TABLE IF EXISTS `snippets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `snippets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_snippets_on_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `snippets`
--

LOCK TABLES `snippets` WRITE;
/*!40000 ALTER TABLE `snippets` DISABLE KEYS */;
/*!40000 ALTER TABLE `snippets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `updates`
--

DROP TABLE IF EXISTS `updates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `updates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT NULL,
  `order` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `updates`
--

LOCK TABLES `updates` WRITE;
/*!40000 ALTER TABLE `updates` DISABLE KEYS */;
INSERT INTO `updates` VALUES (13,'Placement Recognized Workshop','workshops/autoanatomy',1,NULL,'AUTO-ANATOMY Workshop','2014-01-17 11:33:27','2014-01-17 11:33:27'),(15,'Placement Recognized Workshop','workshops/augmentedreality',1,NULL,'REAL-TIME INTERACTIVE MODELLING Workshop','2014-01-17 11:34:47','2014-01-17 11:34:47'),(16,'Placement Recognized Workshop','workshops/adcasting',1,NULL,'ADVANCED INDUSTRIAL CASTING & FORMING Workshop','2014-01-17 11:35:16','2014-01-17 11:35:16'),(18,NULL,NULL,1,NULL,'Various PINNACLE Events Updated!','2014-01-17 11:37:31','2014-01-17 11:37:31'),(20,NULL,'piambassador',1,NULL,'Check out the P! AMBASSADOR PROGRAM!','2014-01-18 15:58:01','2014-01-18 15:58:01');
/*!40000 ALTER TABLE `updates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `encrypted_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oauth_token` text COLLATE utf8_unicode_ci,
  `oauth_expires` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) NOT NULL DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_users_on_email` (`email`),
  UNIQUE KEY `index_users_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'qms@smeannauniv.org','$2a$10$TM1N7bFYZeS.PU6HcgwOt.Cp4fjGqKxjb5F0mGswKn9c1fEHtKPDe',NULL,NULL,NULL,NULL,NULL,NULL,NULL,39,'2014-01-22 15:48:33','2014-01-22 13:50:50','182.65.149.215','122.174.154.8','2014-01-13 14:55:25','2014-01-22 15:48:33','super_admin'),(2,'vkh93@yahoo.co.in','$2a$10$Ef3ZwLP.MJNsBUzFGiMine2kH2QqC577A3XeeTehKtvJtzq7M5jd2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,'2014-01-18 13:25:52','2014-01-13 20:16:55','117.216.40.124','117.193.44.118','2014-01-13 20:16:55','2014-01-18 13:25:52',NULL),(3,'events@smeannauniv.org','$2a$10$RhjJRPi.KdhfBAc3P57C0O7cRtPdXgVUdar7osXm1MG4Vh9EGkKju',NULL,NULL,NULL,NULL,NULL,NULL,NULL,6,'2014-01-18 17:05:25','2014-01-18 16:05:51','117.193.40.224','122.174.86.72','2014-01-15 13:16:36','2014-01-18 17:05:25','event_admin'),(4,'onlinemarketting@smeannauniv.org','$2a$10$xv/N6SI40OAaXsg3HMUqveoTvjXqboJ7IoyyJhqtzAeEhomD5CriW',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2014-01-15 13:19:25','2014-01-15 13:19:25','117.216.44.21','117.216.44.21','2014-01-15 13:19:25','2014-01-15 13:19:25',NULL),(5,'workshop@smeannauniv.org','$2a$10$kbRF3PfNl9eMTyr5rgio1e7.44qb5HY9V1AGNQUHDA4V36s1xquXe',NULL,NULL,NULL,NULL,NULL,NULL,NULL,9,'2014-01-22 14:15:11','2014-01-18 16:04:44','122.174.154.8','122.174.86.72','2014-01-15 13:21:34','2014-01-22 14:15:11','workshop_admin'),(6,'marketing@smeannauniv.org','$2a$10$0jnChmR6RMplKe4aqJK2cObFwB0B1rH0C4qSmTdqYh/aALY1LoR9O',NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,'2014-01-20 09:06:25','2014-01-15 14:05:39','115.249.172.9','117.203.75.14','2014-01-15 14:05:38','2014-01-20 09:06:25',NULL),(7,'ajaiy@tqube.com','$2a$10$McRwuwlKH6A7mNEzw9QEhO5HKxd1b2ETI3z057twDoIby/y6t2Iv6',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2014-01-19 15:02:59','2014-01-19 15:02:59','103.31.214.86','103.31.214.86','2014-01-19 15:02:58','2014-01-19 15:02:59',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workshops`
--

DROP TABLE IF EXISTS `workshops`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workshops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `venue` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `tabs` text COLLATE utf8_unicode_ci,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `cover_image_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cover_image_content_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cover_image_file_size` int(11) DEFAULT NULL,
  `cover_image_updated_at` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workshops`
--

LOCK TABLES `workshops` WRITE;
/*!40000 ALTER TABLE `workshops` DISABLE KEYS */;
INSERT INTO `workshops` VALUES (1,'AUTO-ANATOMY',NULL,'Vivekananda Auditorium, Anna University, Chennai-25',NULL,'2014-01-13 17:13:13','2014-01-19 07:03:23','[{\"title\":\"INTRODUCTION\",\"content\":\"Automotive technology is rapidly evolving on a day to day basis. Keeping up with the latest happenings in this industry is of vital importance to any mechanical engineer. Each vehicle may have its own unique characteristics, but the overall structure is still the same for all. An engine to pump the fuel, an ECU to control the actions, transmission system to transmit the enormous amount of power produced and the body to give the perfect look. It works as a human body; isn’t it fascinating?!\\nThe similarities go even deeper - every automobile has a life in it where it pumps its blood-so called ‘fuel’ from its heart, that is, the engine. The anatomy of an automobile resembles a human body. All those who are of the opinion that car is their life, this workshop is an absolute must. After all, one should better be closely acquainted with their life.\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003EThe Society of Mechanical Engineers present “\\u003Cb\\u003EAUTO-ANATOMY\\u003C/b\\u003E”, a One-Day \\u003Cb\\u003EPlacement Recognized Workshop\\u003C/b\\u003E, a complete focus on the automobile structure and its parts. Auto Anatomy welcomes you to the world of automobiles to obtain complete knowledge about automobiles by presented to you by experienced speakers from renowned leading automotive companies such as \\u003Cb\\u003ESimpsons Co Ltd., Ashok Leyland Ltd., Mahindra \\u0026amp; Mahindra Ltd., Renault-Nissan Ltd., WABCO-TVS Ltd and concluded by a great automotive enthusiast and a Professor from Anna University, Chennai.\\u003C/b\\u003E The overall technical objective of the workshop is the achievement of the various vehicle configuration from the Engine System to the Chassis-Transmission System while showing it in Live-Demos.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003EThe participant after attending the workshop will help choose ones-self to enter into the field of Automotive Technology and develop an un-explainable love for Automobiles.\\u003C/div\\u003E\"},{\"title\":\"SPEAKERS\",\"content\":\"To be updated soon.\"},{\"title\":\"SESSIONS\",\"content\":\"To be updated soon.\"},{\"title\":\"RULES\",\"content\":\"\\u003Cdiv\\u003E\\u003Cb\\u003E\\u003Cu\\u003EELIGIBILITY\\u003C/u\\u003E:\\u003C/b\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003EStudents with valid ID cards from recognized educational institutions are eligible for the workshop\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cb\\u003E\\u003Cu\\u003ETYPE OF PARTICIPATION \\u0026amp; REGISTRATION FEE\\u003C/u\\u003E:\\u003C/b\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003EEach participant can register only individually .The registration fee of the workshop is as follows.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv style=\\\"text-align: center;\\\"\\u003E\\u003Cb\\u003ESingle\\u003Cspan class=\\\"Apple-tab-span\\\" style=\\\"white-space: pre;\\\"\\u003E\\t\\t\\t\\u003C/span\\u003E- Rs. 1200/-\\u0026nbsp;\\u003C/b\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cdiv style=\\\"text-align: center;\\\"\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003Ewhich includes the Kit, Training Charges, Demonstration and Lunch. The details of payment for the workshops will be updated soon.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cb\\u003E\\u003Cu\\u003EKIT DETAILS\\u003C/u\\u003E:\\u003C/b\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cb\\u003E\\u003Cbr\\u003E\\u003C/b\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cul\\u003E\\u003Cli\\u003EPINNACLE Workshop Hand-Kit.\\u003C/li\\u003E\\u003Cli\\u003EOfficial Book on \\u0026nbsp;\\\"Principles, Guides \\u0026amp; Briefs on Automotive Technology\\\" by Society of Mechanical Engineers, Anna University, Ch-25.\\u003C/li\\u003E\\u003C/ul\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cb\\u003E\\u003Cu\\u003ECERTIFICATION\\u003C/u\\u003E:\\u003C/b\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cb\\u003E\\u003Cbr\\u003E\\u003C/b\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003EParticipants must attend all the sessions to obtain the Placement Recognized Workshop Certificate from SME, Anna University. Failing to do so, the participant\'s registration will stand cancelled and the participant will also have to forfeit the Registration Fee.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\"},{\"title\":\"REGISTRATION\",\"content\":\"\\u003Cdiv\\u003ELimited Rolling Registrations.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003ERegistration for this Workshop will be updated soon.\\u003Cbr\\u003E\"},{\"title\":\"FAQs\",\"content\":\"\\u003Cdiv\\u003E1). Should I bring any material for this Workshop?\\u003C/div\\u003E\\u003Cdiv\\u003EYou must bring your College ID card\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E2). What are the pre-requisites for attending this Workshop?\\u003C/div\\u003E\\u003Cdiv\\u003EWe expect you to come with a sincere dedication and interest in the field of this workshop. Apart form this, this workshop is tuned in such a way that it can be attended by a beginner or an advanced level participant.\\u0026nbsp;We also require you to be present in pure formals, neatly attired and maintain discipline at all times.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E3). How will I be benefited with this Workshop?\\u003C/div\\u003E\\u003Cdiv\\u003ESME has been uniquely conducted Workshops for decades. We have brilliant speakers from\\u0026nbsp;Simpsons Co Ltd., Ashok Leyland Ltd., Mahindra \\u0026amp; Mahindra Ltd., Renault-Nissan Ltd., WABCO-TVS Ltd and Anna University along with Live-Demonstration. Students aspiring to enter into the Automobile Sector have a wider chance to secure themselves with a good placement in their respective college.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E4). Do you provide accommodation for this Workshop?\\u003C/div\\u003E\\u003Cdiv\\u003EYes. If you need accommodation, kindly visit the hospitality tab in the website. You should register separately and pay for the accommodation.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E5). How do I register for this Workshop?\\u003C/div\\u003E\\u003Cdiv\\u003EYou will need to register as a participant by Login. Then, please visit the \\u003Cb\\u003EREGISTRATION\\u003C/b\\u003E Tab for more details.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E6). What is the mode of payment for this Workshop?\\u003C/div\\u003E\\u003Cdiv\\u003EThe only mode of payment for the workshop will be through \\u003Cb\\u003EDemand Draft.\\u003C/b\\u003E Please visit the \\u003Cb\\u003EREGISTRATION\\u003C/b\\u003E Tab for more details.\\u003C/div\\u003E\"},{\"title\":\"CONTACT\",\"content\":\"For queries:\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cb\\u003Eworkshop@smeannauniv.org\\u003C/b\\u003E\\u003C/div\\u003E\"}]','/workshops/autoanatomy','null','2014-03-15','2014-03-15','08:30:00','auto_anatomy.png','image/png',557624,'2014-01-16 18:07:20',NULL),(2,'ADVANCED INDUSTRIAL CASTING & FORMING',NULL,'CEG Tag Auditorium, CEG Campus',NULL,'2014-01-13 17:22:58','2014-01-20 09:33:23','[{\"title\":\"INTRODUCTION\",\"content\":\"Everything we see or touch has its own story of how it was made. From the toothpaste we use to the bed we sleep on, all of them have their very own evolution process and we are here to provide you with the insight of this making process.\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003EFor the first time in the history of CEG, Society of Mechanical Engineers proudly presents to you a\\u0026nbsp;\\u003Cb\\u003EPlacement-Recognized Workshop\\u003C/b\\u003E\\u0026nbsp;on\\u0026nbsp;\\u003Cb\\u003E“ADVANCED INDUSTRIAL CASTING AND FORMING”\\u003C/b\\u003E. This is a one of a kind workshop providing you a complete insight about various casting and forming of different engineering components.The Two-Day workshop facilitates in providing you a thorough knowledge on the progression and the intricacy of the casting and forming techniques.The workshop will also be providing you with live visualization components and copies of engineering drawings that are the end result of those manufacturing processes, where you can understand the actual process in greater detail.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003EYou will even be able to view the dies used in the processes, kept in the workshop during demonstration for your further benefit. We will have our expert speakers from reputed companies who will certainly give you an experience of a life time! So do enroll and be the difference the companies are looking for during your placements.\\u003C/div\\u003E\"},{\"title\":\"SPEAKERS\",\"content\":\"To be updated soon.\"},{\"title\":\"SESSIONS\",\"content\":\"To be updated soon.\"},{\"title\":\"DEMONSTRATION\",\"content\":\"To be updated soon.\"},{\"title\":\"RULES\",\"content\":\"\\u003Cdiv\\u003E\\u003Cb\\u003E\\u003Cu\\u003EELIGIBILITY\\u003C/u\\u003E:\\u003C/b\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003EStudents with valid ID cards from recognized educational institutions are eligible for the workshop\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cb\\u003E\\u003Cu\\u003ETYPE OF PARTICIPATION \\u0026amp; REGISTRATION FEE\\u003C/u\\u003E:\\u003C/b\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003EEach participant can register only individually .The registration fee of the workshop is as follows.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv style=\\\"text-align: center;\\\"\\u003E\\u003Cb\\u003ESingle\\u003Cspan class=\\\"Apple-tab-span\\\" style=\\\"white-space: pre;\\\"\\u003E\\t\\t\\t\\u003C/span\\u003E- Rs. 2000/-\\u003C/b\\u003E\\u003C/div\\u003E\\u003Cdiv style=\\\"text-align: center;\\\"\\u003E\\u003Cb\\u003ETeam (of 2) \\u0026nbsp; \\u0026nbsp; \\u0026nbsp; \\u0026nbsp; \\u0026nbsp; \\u0026nbsp;- \\u0026nbsp;Rs. 3800/-\\u003C/b\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cdiv style=\\\"text-align: center;\\\"\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003Ewhich includes the Kit, Training Charges, Demonstration and Lunch \\u003Ci\\u003E(one day\\u003C/i\\u003E). The details of payment for the workshops will be updated soon.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cb\\u003E\\u003Cu\\u003EKIT DETAILS\\u003C/u\\u003E:\\u003C/b\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cb\\u003E\\u003Cbr\\u003E\\u003C/b\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cul\\u003E\\u003Cli\\u003EPINNACLE Workshop Hand-Kit.\\u003C/li\\u003E\\u003Cli\\u003EActual Pressure Die Casting Component and Plastic Mold Component.\\u003C/li\\u003E\\u003Cli\\u003EOfficial Copies of Engineering Drawings of the various Forged and Casted Components during the Live-Demonstration.\\u003C/li\\u003E\\u003C/ul\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cb\\u003E\\u003Cu\\u003ECERTIFICATION\\u003C/u\\u003E:\\u003C/b\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cb\\u003E\\u003Cbr\\u003E\\u003C/b\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003EParticipants must attend all the sessions to obtain the Placement Recognized Workshop Certificate from SME, Anna University. Failing to do so, the participant\'s registration will stand cancelled and the participant will also have to forfeit the Registration Fee.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\"},{\"title\":\"REGISTRATION\",\"content\":\"\\u003Cdiv\\u003ELimited Rolling Registrations.\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003ERegistration for this Workshop will be updated soon.\\u003Cdiv\\u003E\\u003C/div\\u003E\"},{\"title\":\"FAQs\",\"content\":\"\\u003Cdiv\\u003E1). Should I bring any material for this Workshop?\\u003C/div\\u003E\\u003Cdiv\\u003EYou must bring your College ID card.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E2). How long is this Workshop?\\u003C/div\\u003E\\u003Cdiv\\u003EThis is a 2-day workshop consisting of theoretical sessions and live demonstration.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E3). What are the pre-requisites for attending this Workshop?\\u003C/div\\u003E\\u003Cdiv\\u003EWe expect you to come with a sincere dedication and interest in the field of this workshop. Apart form this, this workshop is tuned in such a way that it can be attended by a beginner or an advanced level participant.\\u0026nbsp;We also require you to be present in pure formals, neatly attired, \\u003Cb\\u003Eworkshop shoes\\u003C/b\\u003E and maintain discipline at all times.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E4). How will I be benefited with this Workshop?\\u003C/div\\u003E\\u003Cdiv\\u003ESME has been uniquely conducted Workshops for decades. We have brilliant speakers from\\u0026nbsp;Hinduja Foundries \\u0026amp; Central Institute of Plastics Engineering and Technology along with Live-Demonstration of various Dies and Components. Students aspiring to enter into the Manufacturing Sector have a wider chance to secure themselves with a good placement in their respective college.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E5). Do you provide accommodation for this Workshop?\\u003C/div\\u003E\\u003Cdiv\\u003EYes. If you need accommodation, kindly visit the hospitality tab in the website. You should register separately and pay for the accommodation.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E6). How do I register for this Workshop?\\u003C/div\\u003E\\u003Cdiv\\u003EYou will need to register as a participant by Login. Then, please visit the\\u0026nbsp;\\u003Cb\\u003EREGISTRATION\\u003C/b\\u003E\\u0026nbsp;Tab for more details.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E7). What is the mode of payment for this Workshop?\\u003C/div\\u003E\\u003Cdiv\\u003EThe only mode of payment for the workshop will be through\\u0026nbsp;\\u003Cb\\u003EDemand Draft.\\u003C/b\\u003E\\u0026nbsp;Please visit the\\u0026nbsp;\\u003Cb\\u003EREGISTRATION\\u003C/b\\u003E\\u0026nbsp;Tab for more details.\\u003C/div\\u003E\"},{\"title\":\"CONTACT\",\"content\":\"For queries:\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cb\\u003Eworkshop@smeannauniv.org\\u003C/b\\u003E\\u003C/div\\u003E\"}]','/workshops/adcasting','null','2014-03-13','2014-03-14','09:00:00','foundries.png','image/png',746302,'2014-01-16 06:58:08',NULL),(3,'REAL-TIME INTERACTIVE MODELLING',NULL,'Henry Maudslay Hall, Department of Mechanical Engineering, CEG Campus',NULL,'2014-01-13 17:25:28','2014-01-19 07:05:00','[{\"title\":\"INTRODUCTION\",\"content\":\"Are you interested to know about how the real world environment interacts with virtual objects? This is then a chance for you to learn about augmented reality. Now, what is augmented reality? It is a virtual reality that aims to duplicate the world’s environment in a computer.\\u0026nbsp;\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003EThe Society of Mechanical Engineers in association with PupaClic presents a \\u003Cb\\u003EPlacement-Recognised Workshop\\u003C/b\\u003E on “\\u003Cb\\u003EREAL-TIME INTERACTIVE MODELLING\\u003C/b\\u003E”. \\n     This workshop will be a fabulous opportunity for you to add an extra feather to your cap.\\u0026nbsp;You will first be walked through the basics of augmented reality and its applications. Later on, you will be having a session over the extensive use of \\u003Cb\\u003EWeb AR\\u003C/b\\u003E and  \\u003Cb\\u003EUNITY 3D\\u003C/b\\u003E in a lucid manner, with finally a showcase on Augmented Reality.\\nAugmented Reality brings new dimensions for learning. Knowledge of AR unlocks or creates layers of digital information on top of the physical world that can be viewed through an Android or an iOS service. Applications for AR are broad - navigation, military, sightseeing, medical, etc. Learning augmented reality will help you to enhance the bridge that spans from the real onto the digital world!\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003EAt the end of the workshop, you will leave with the broad understanding of augmented reality and a greater sense of how this technology can be. Don’t miss out this wonderful opportunity of becoming an expert in augmented reality.\\u003C/div\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\"},{\"title\":\"SPEAKERS\",\"content\":\"To be updated soon.\"},{\"title\":\"SESSIONS\",\"content\":\"\\u003Cdiv\\u003E\\u003Cb\\u003E\\u0026nbsp; \\u0026nbsp;\\u0026nbsp;\\u003Cu\\u003ETIMINGS \\u003C/u\\u003E\\u0026nbsp; \\u0026nbsp; \\u0026nbsp; \\u0026nbsp; \\u0026nbsp; \\u0026nbsp; \\u0026nbsp; \\u0026nbsp; \\u0026nbsp; \\u0026nbsp; \\u0026nbsp; \\u0026nbsp; \\u0026nbsp; \\u0026nbsp; \\u0026nbsp; \\u0026nbsp; \\u0026nbsp; \\u0026nbsp; \\u0026nbsp;\\u003Cu\\u003ESESSIONS\\u003C/u\\u003E\\u003C/b\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E9:00-10:00 am\\u003Cspan class=\\\"Apple-tab-span\\\" style=\\\"white-space:pre\\\"\\u003E\\t\\u003C/span\\u003E-\\u0026nbsp;Introduction- What is Augmented Reality (AR), Its Engineering Applications. (BMW, Civil builders)(Exclusive Footage of Videos)\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cb\\u003E\\u003Cbr\\u003E\\u003C/b\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E10:00-1:00 pm\\u003Cb\\u003E\\u0026nbsp;\\u003C/b\\u003E-Basic Level 3D Modeling Workshop using Google Sketch-up (Engine, Gearbox \\u0026amp; Steering) and \\u0026nbsp;CREO Elements/Pro\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cb\\u003E\\u003Cbr\\u003E\\u003C/b\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E1:00- 2:00 pm\\u003Cspan class=\\\"Apple-tab-span\\\" style=\\\"white-space:pre\\\"\\u003E\\t\\u003C/span\\u003E- LUNCH (All participants \\u0026amp; Organizers)\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cb\\u003E\\u003Cbr\\u003E\\u003C/b\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E2:00- 2:30 pm\\u003Cb\\u003E \\u003C/b\\u003E-\\u003Cb\\u003E\\u0026nbsp;\\u003C/b\\u003ELAYAR – Creating Interactive Print Augmented Reality\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cb\\u003E\\u003Cbr\\u003E\\u003C/b\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E2:30- 3:30 pm\\u003Cspan class=\\\"Apple-tab-span\\\" style=\\\"white-space:pre\\\"\\u003E\\t\\u003C/span\\u003E- Web AR – Creating HTML based Augmented Reality Application\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cb\\u003E\\u003Cbr\\u003E\\u003C/b\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E3:30- 5:00 pm\\u003Cspan class=\\\"Apple-tab-span\\\" style=\\\"white-space:pre\\\"\\u003E\\t\\u003C/span\\u003E- Unity 3D – Creating Augmented Reality Mobile Applications for iOS and Android\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cb\\u003E\\u003Cbr\\u003E\\u003C/b\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E5:00- 6:00 pm\\u003Cspan class=\\\"Apple-tab-span\\\" style=\\\"white-space:pre\\\"\\u003E\\t\\u003C/span\\u003E- AR Showcase- Audi R8, Iron Man etc.\\u0026nbsp;\\u003C/div\\u003E\"},{\"title\":\"RULES\",\"content\":\"\\u003Cdiv\\u003E\\u003Cb\\u003EELIGIBILITY:\\u003C/b\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003EStudents with valid ID cards from recognized educational institutions are eligible for the workshop\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cb\\u003ETYPE OF PARTICIPATION \\u0026amp; REGISTRATION FEE:\\u003C/b\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003EEach participant can register individually or as a team of 2 members. The registration fee of the workshop is as follows.\\u003C/div\\u003E\\u003Cdiv style=\\\"text-align: center;\\\"\\u003E\\u003Cb\\u003ESingle\\u003Cspan class=\\\"Apple-tab-span\\\" style=\\\"white-space:pre\\\"\\u003E\\t\\t\\t\\u003C/span\\u003E- Rs. 1300/-\\u003C/b\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cdiv style=\\\"text-align: center;\\\"\\u003E\\u003Cb\\u003ETeam (of 2)\\u003Cspan class=\\\"Apple-tab-span\\\" style=\\\"white-space:pre\\\"\\u003E\\t\\t\\u003C/span\\u003E- Rs. 2000/-\\u003C/b\\u003E\\u003C/div\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003Ewhich includes the Kit, Training charges and lunch. The details of payment for the workshops will be updated soon.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cb\\u003EKIT DETAILS:\\u003C/b\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cul\\u003E\\u003Cli\\u003EPINNACLE Workshop Hand-Kit.\\u003C/li\\u003E\\u003Cli\\u003EOfficial DVD consisting of licensed following softwares from PupaClic Inc.\\u003C/li\\u003E\\u003C/ul\\u003E\\u003Col\\u003E\\u003Cli\\u003E\\u003Cspan\\u003EUnity 3D - Evaluation copy\\u003C/span\\u003E\\u003C/li\\u003E\\u003Cli\\u003EPupa Clic - Web AR SDK\\u003C/li\\u003E\\u003Cli\\u003EWorkshop Support Documentation\\u003C/li\\u003E\\u003Cli\\u003EAR Marker Creator and Plugins\\u003C/li\\u003E\\u003Cli\\u003EClic AR SDK Unity Plugin\\u003C/li\\u003E\\u003C/ol\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cb\\u003ECERTIFICATION:\\u003C/b\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cb\\u003E\\u003Cbr\\u003E\\u003C/b\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003EParticipants must attend all the sessions to obtain the Placement Recognized Workshop Certificate from SME, Anna University. Failing to do so, the participant\'s registration will stand cancelled and the participant will also have to forfeit the Registration Fee.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\"},{\"title\":\"REQUIREMENTS\",\"content\":\"\\u003Cdiv\\u003E\\u003Cb\\u003ECOMPULSORY HARDWARE \\u0026amp; SOFTWARE REQUIREMENTS\\u003C/b\\u003E\\u003Cb\\u003E:\\u0026nbsp;\\u003C/b\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cul\\u003E\\u003Cli\\u003EWindows XP SP2 or later; Windows 7 SP1; Mac OS X \\\"Snow Leopard\\\" 10.6 or later.\\u0026nbsp;\\u003C/li\\u003E\\u003Cli\\u003EBasic Graphics card with DirectX 9 level (Shader model 2.0) capabilities. Any card made since 2004 should work.\\u003C/li\\u003E\\u003Cli\\u003EAndroid with OS 4.0 or more supported Mobile.\\u003C/li\\u003E\\u003Cli\\u003EAndroid SDK and Java Development Kit (JDK) install from here:\\u0026nbsp;\\u003C/li\\u003E\\u003C/ul\\u003E\\u003Cdiv style=\\\"text-align: center;\\\"\\u003Ehttp://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html\\u003C/div\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cb\\u003ENOTE:\\u003C/b\\u003E\\u0026nbsp;Unity was not tested on server versions of Windows and OS X.\\u003C/div\\u003E\"},{\"title\":\"REGISTRATION\",\"content\":\"\\u003Cdiv\\u003ELimited Rolling Registrations.\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003ERegistration for this Workshop will be updated soon.\"},{\"title\":\"FAQs\",\"content\":\"\\u003Cdiv\\u003E1). Should I bring any material for this Workshop?\\u003C/div\\u003E\\u003Cdiv\\u003EYou must bring your College ID card.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E2). What are the pre-requisites for attending this Workshop?\\u003C/div\\u003E\\u003Cdiv\\u003EWe expect you to come with a sincere dedication and interest in the field of this workshop. Apart form this, this workshop is tuned in such a way that it can be attended by a beginner or an advanced level participant.\\u0026nbsp;We also require you to be present in pure formals, neatly attired and maintain discipline at all times.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E3). How will I be benefited with this Workshop?\\u003C/div\\u003E\\u003Cdiv\\u003ESME has been uniquely conducted Workshops for decades. We have brilliant speakers from PupaClic, one of the most innovative start-ups which specializes in Augmented Reality. This is along with Live-Demonstration. Students aspiring to enter into the Designing and Modelling Sector have a wider chance to secure themselves with a good placement in their respective college.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E4). Do you provide accommodation for this Workshop?\\u003C/div\\u003E\\u003Cdiv\\u003EYes. If you need accommodation, kindly visit the hospitality tab in the website. You should register separately and pay for the accommodation.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E5). How do I register for this Workshop?\\u003C/div\\u003E\\u003Cdiv\\u003EYou will need to register as a participant by Login. Then, please visit the\\u0026nbsp;\\u003Cb\\u003EREGISTRATION\\u003C/b\\u003E\\u0026nbsp;Tab for more details.\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E6). What is the mode of payment for this Workshop?\\u003C/div\\u003E\\u003Cdiv\\u003EThe only mode of payment for the workshop will be through \\u003Cb\\u003EDemand Draft\\u003C/b\\u003E. Please visit the\\u0026nbsp;\\u003Cb\\u003EREGISTRATION\\u003C/b\\u003E\\u0026nbsp;tab for more details.\\u003C/div\\u003E\"},{\"title\":\"CONTACT\",\"content\":\"For queries:\\u003Cdiv\\u003E\\u003Cbr\\u003E\\u003C/div\\u003E\\u003Cdiv\\u003E\\u003Cb\\u003Eworkshop@smeannauniv.org\\u003C/b\\u003E\\u003C/div\\u003E\"}]','/workshops/augmentedreality','null','2014-03-14','2014-03-14','09:00:00','AR.png','image/png',622288,'2014-01-18 08:30:36',NULL);
/*!40000 ALTER TABLE `workshops` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-01-22 15:50:13
