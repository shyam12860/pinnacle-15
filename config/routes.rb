Bpgc::Application.routes.draw do

  # Rebus
  get '/rebus' => "rebus#index"
  get '/rebus/game' => "rebus#game"
  post '/rebus/answer' => "rebus#check_answer"
  get '/rebus/test' => "rebus#test_rebus"
  get '/rebus/skip' => "rebus#skip_question"
  get '/rebus/leaderboard' => "rebus#leaderboard"
  
  get "cms/rebus" => "cms_rebus#index", as: :cms_rebus
  get "cms/rebuss" => "cms_rebus#show", as: :cms_rebus_show
  post "cms/rebus" => "cms_rebus#save", as: :cms_rebus_save
  put "cms/rebus" => "cms_rebus#update", as: :rebus_update
  delete "cms/rebus/:round" => "cms_rebus#delete", as: :rebus_delete



  get "cms/sponsor" => "cms_sponsor#index", as: :sponor
  get "cms_sponsor/save"
  get "cms_sponsor/show"
  get "cms_sponsor/update"
  get "cms_sponsor/delete"
  get "cms_sponsor/file_upload"

  mount Ckeditor::Engine => '/ckeditor'
  devise_for :users,controllers: { omniauth_callbacks: "users/omniauth_callbacks" }

  devise_scope :user do
    get 'register', to: 'devise/registrations#new', as: :register
    get 'login', to: 'devise/sessions#new', as: :login
    get 'logout', to: 'devise/sessions#destroy', as: :logout
  end
  get "cms/" => "cms_main#index", as: :dash
  get "cms/registrations" => "cms_main#viewreg", as: :viewreg
  get "cms/change_payment/:id" => "cms_main#changepayment", as: :changepayment
  post "cms/registrations" => "cms_main#viewreg", as: :viewreg_post
  post "cms/regxls" => "cms_main#viewxls", as: :viewxls_post
  get "cms/regxls" => "cms_main#viewxls", as: :viewxls
  get "cms/menu" => "cms_main#menu", as: :menu
  post 'cms/menu' => 'cms_main#save_menu', as: :save_menu
  delete 'cms/menu/:id' => 'cms_main#delete_menu', as: :delete_menu
  delete "cms/delete_reg/:id" => 'cms_main#delete_reg', as: :delete_reg

  get 'cms/piambassador' => 'cms_main#piambassador', as: :cms_piambassador
  get 'cms/viewpi/:referralid' => 'cms_main#viewpi', as: :cms_viewpi
  
  #page cms
  get "cms/settings" => "cms_settings#index", as: :settings_cms
  get "cms/setting" => "cms_settings#show", as: :settings_show

  #page cms
  get "cms/category" => "cms_category#index", as: :category_cms
  post "cms/category" => "cms_category#save", as: :category_save
  get "cms/categories" => "cms_category#show", as: :category_show
  post "cms/category/url" => "cms_category#checkurl", as: :category_url
  get "cms_category/update"
  delete "cms/category/:id" => "cms_category#delete", as: :category_delete

  #page cms
  get "cms/workshop" => "cms_workshop#index", as: :workshop_cms
  post "cms/workshop" => "cms_workshop#save", as: :workshop_save
  get "cms/workshops" => "cms_workshop#show", as: :workshop_show
  get "cms/workshops/:id" => 'cms_workshop#show', as: :workshop
  delete "cms/workshop/:id" => "cms_workshop#delete", as: :workshop_delete
  put "cms/workshop" => 'cms_workshop#update', as: :workshop_update
  post 'cms/workshop/url' => 'cms_workshop#checkurl', as: :workshop_url
  post 'cms/workshops/file' => 'cms_workshop#file_upload', as: :workshop_file

  #page cms
  get "cms/sponsor" => "cms_sponsor#index", as: :sponsor_cms
  post "cms/sponsor" => "cms_sponsor#save", as: :sponsor_save
  get "cms/sponsors" => "cms_sponsor#show", as: :sponsor_show
  delete "cms/sponsor/:id" => "cms_sponsor#delete", as: :sponsor_delete
  get "cms_sponsor/file_upload"


  #page cms
  get "cms/slider" => "cms_slider#index", as: :slider_cms
  post "cms/slider" => "cms_slider#save", as: :slider_save
  get "cms/sliders" => "cms_slider#show", as: :slider_show
  delete "cms/slider/:id" => "cms_slider#delete", as: :slider_delete
  get "cms_slider/file_upload"

  get "main/index"
  get "main/home"
  get "main/updates" => 'main#updates', as: :main_update
  get "main/slides" => 'main#slides', as: :main_slide
  get "main/sponsors" => 'main#sponsors', as: :main_sponsor
  get "main/category" => 'main#category', as: :main_category
  get "main/menu" => 'main#menu', as: :main_menu
  post "main/saveprofile" => 'main#saveprofile', as: :main_saveprofile
  get "main/getprofile" => 'main#getprofile', as: :main_getprofile
  post "main/getregistration" => 'main#getregistration', as: :main_getregistration
  get "/referral/:referralid" => 'main#referral', as: :main_referral

  #page cms
  get "cms/page" => 'cms_page#index', as: :page_cms
  get "cms/pages" => 'cms_page#show', as: :pages
  get "cms/page/:id" => 'cms_page#show', as: :page
  post "cms/page" => 'cms_page#save', as: :page_save
  put "cms/page" => 'cms_page#update', as: :page_update
  delete "cms/page/:id" => 'cms_page#delete', as: :page_delete
  post "cms/page/url" => "cms_page#checkurl", as: :page_url


  #page cms
  get "cms/update" => 'cms_update#index', as: :update_cms
  get "cms/updates" => 'cms_update#show', as: :updates
  get "cms/update/:id" => 'cms_update#show', as: :update
  post "cms/update" => 'cms_update#save', as: :update_save
  put "cms/update" => 'cms_update#update', as: :update_update
  delete "cms/update/:id" => 'cms_update#delete', as: :update_delete

  #page cms 
  get "cms/event" => 'cms_event#index', as: :event_cms
  get "cms/events" => 'cms_event#show', as: :events
  
  get "cms/events/:id" => 'cms_event#show', as: :event
  post "cms/event" => 'cms_event#save', as: :event_save
  put "cms/event" => 'cms_event#update', as: :event_update
  delete "cms/event/:id" => 'cms_event#delete', as: :event_delete
  #manage users
  get 'manage/user' => 'manageuser#index', as: :user_index
  get 'manage/user/list' => 'manageuser#show' 
  put 'manage/user/update' => 'manageuser#update'
  delete 'manage/user/delete/:id' => 'manageuser#delete'
  get 'manage/user/:id' => 'manageuser#assign'

  post 'cms/event/url' => 'cms_event#checkurl', as: :event_url
  post 'cms/events/file' => 'cms_event#file_upload', as: :event_file

  get '/profile' => 'main#profile', as: :profile

  get ':id0'=>'main#page', as: :get_page_0
  get ':id0/:id1'=>'main#page', as: :get_page_1
  get ':id0/:id1/:id2'=>'main#page', as: :get_page_2
  get ':id0/:id1/:id2/:id3'=>'main#page', as: :get_page_3
  get ':id0/:id1/:id2/:id3/:id4'=>'main#page', as: :get_page_4
  get ':id0/:id1/:id2/:id3/:id4/:id5'=>'main#page', as: :get_page_5
  get ':id0/:id1/:id2/:id3/:id4/:id5/:id6'=>'main#page', as: :get_page_6
  get ':id0/:id1/:id2/:id3/:id4/:id5/:id6/:id7'=>'main#page', as: :get_page_7
  
  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'main#index'
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
